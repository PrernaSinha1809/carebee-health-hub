import React, { Component } from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPreset, CardStyleInterpolators } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
// Importing Screens --------------------------------------------->

import OpenPage from '../screens/OpenPage'
import ForgetPassword from '../screens/ForgetPassword'
import OTPrecieved from '../screens/OTPRecieved';
import OnBoarding1 from '../screens/OnBoarding1';
import OnBoarding2 from '../screens/OnBoarding2';
import OnBoarding3 from '../screens/OnBoarding3';
import Diabetes from '../screens/HealthTipArticles/Diabetes';
import Vitals from '../screens/Vitals';
import Homescreen from '../screens/Homescreen';
import Dashboard from '../screens/Dashboard';
import Profile from '../screens/Profile';
import HealthTips from '../screens/HealthTips';
import MentalHealth from '../screens/HealthTipArticles/MentalHealth';
import Stress from '../screens/HealthTipArticles/Stress';
import BloodOxygen from '../screens/DashboardScreens/Blood_Oxygen';
import BloodSugar from '../screens/DashboardScreens/Blood_Sugar';
import HeartRate from '../screens/DashboardScreens/Heart_Rate';
import Weight from '../screens/DashboardScreens/Weight';
import EditProfile from '../screens/EditProfile';
import MyPrescription from '../screens/HomeDrawer/MyPrescription';
import Reminder from '../screens/HomeDrawer/Reminder';
import MentalHealthSplash from '../screens/MentalHealthScreens/MentalHealthIntro';
import MentalHealthQuest from '../screens/MentalHealthScreens/MentalHealth';
import MentalHealth1 from '../screens/MentalHealthScreens/MentalHealth1';
import MentalHealth2 from '../screens/MentalHealthScreens/MentalHealth2';
import MentalHealth3 from '../screens/MentalHealthScreens/MentalHealth3';
import AssessmentTest from '../screens/MentalHealthScreens/AssessmentTest';
import AnxietyTest from '../screens/MentalHealthScreens/AnxietyTest1';
import AnxietyTest2 from '../screens/MentalHealthScreens/AnxietyTest2';
import AnxietyTest3 from '../screens/MentalHealthScreens/AnxietyTest3';
import AnxietyTest4 from '../screens/MentalHealthScreens/AnxietyTest4';
import DepressionTest from '../screens/MentalHealthScreens/DepressionTests/DepressionTest';
import DepressionTest2 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest2';
import MentalHCalculator from '../screens/Components/MentalHealthCalculator';
import AnxietyTest5 from '../screens/MentalHealthScreens/AnxietyTest5';
import AnxietyTest6 from '../screens/MentalHealthScreens/AnxietyTest6';
import DepressionTest3 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest3';
import DepressionTest4 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest4';
import DepressionTest5 from '../screens/MentalHealthScreens/DepressionTests/DepressionalTest5';
import DepressionTest6 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest6';
import DepressionTest7 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest7'
import DepressionTest8 from '../screens/MentalHealthScreens/DepressionTests/DepressionTest8';


// initialising Navigation ---------------------------------------->
const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()
const Tab = createMaterialBottomTabNavigator()

export default class Route extends Component {
    render() {
        return (
            <NavigationContainer>
                {/* <Drawer.Navigator initialRouteName="Home"> */}
                <Drawer.Navigator initialRouteName="OpenPage">
                    <Drawer.Screen name="OpenPage" component={MyStack} />
                </Drawer.Navigator>

            </NavigationContainer>

        );
    }
}

const MyStack = () => {
    return (
        <Stack.Navigator>

            <Stack.Screen name="OpenPage" component={OpenPage} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="ForgetPassword" component={ForgetPassword} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="OTPRecieved" component={OTPrecieved} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="OnBoarding1" component={OnBoarding1} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="OnBoarding2" component={OnBoarding2} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="OnBoarding3" component={OnBoarding3} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Diabetes" component={Diabetes} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHealth" component={MentalHealth} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="stress" component={Stress} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Blood_Oxygen" component={BloodOxygen} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Blood_Sugar" component={BloodSugar} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Heart_Rate" component={HeartRate} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Weight" component={Weight} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="EditProfile" component={EditProfile} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Homescreen" component={BottomNav} options={{
                headerShown: false,
            }} />
        </Stack.Navigator>
    )
}

const BottomNav = () => {
    return (
        <Tab.Navigator initialRouteName="Homescreen" style={{ backgroundColor: 'white' }} barStyle={{ backgroundColor: 'white' }}>
            <Tab.Screen
                name="Homescreen"
                component={HomeScreenFunc}
                options={{
                    tabBarIcon: ({ color }) => (
                        <View style={{}}>
                            <Icon
                                color={color}
                                size={20}
                                name={'home'}
                            />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="Vitals"
                component={Vitals}
                options={{
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Icon
                                color={color}
                                size={20}
                                name={'heart'}
                            />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="Dashboad"
                component={Dashboard}
                options={{
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Icon
                                color={color}
                                size={20}
                                name={'pulse'}
                            />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="HealthTips"
                component={HealthTips}
                options={{
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Icon
                                color={color}
                                size={20}
                                name={'image'}
                            />
                        </View>
                    ),
                }}
            />

            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarIcon: ({ color }) => (
                        <View>
                            <Icon
                                color={color}
                                size={20}
                                name={'radio-button-on'}
                            />
                        </View>
                    ),
                }}
            />
        </Tab.Navigator>
    )
}



const HomeScreenFunc = () => {
    return (
        <Drawer.Navigator initialRouteName="Homescreen" >
            <Drawer.Screen name="Medical history" component={Homescreen} />
            <Drawer.Screen name="Update Profile" component={EditProfile} />
            <Drawer.Screen name="My Prescriptions" component={MyPrescription} />
            <Drawer.Screen name="Reminders" component={Reminder} />
            <Drawer.Screen name="Mental Health" component={MentalHealthScreen} />
            <Drawer.Screen name="Settings" component={Homescreen}/>
        </Drawer.Navigator>
    )
}

const config = {
    animation: 'fade',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

const MentalHealthScreen = () => {
    return(
        <Stack.Navigator screenOptions={{
            gestureEnabled: true,
            gestureDirection: 'horizontal',
            cardStyleInterpolator: CardStyleInterpolators.forScaleFromCenterAndroid
            // transitionSpec: {
            //     open: config,
            //     close: config
            // },
            // headerMode="float",
            // animation="fade"
        }}>
            <Stack.Screen name="Mental Health" component={MentalHealthSplash} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="Homescreen" component={HomeScreenFunc} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHealthQuest" component={MentalHealthQuest} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHealth1" component={MentalHealth1} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHealth2" component={MentalHealth2} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHealth3" component={MentalHealth3} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AssessmentTest" component={AssessmentTest} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest" component={AnxietyTest} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest2" component={AnxietyTest2} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest3" component={AnxietyTest3} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest4" component={AnxietyTest4} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest5" component={AnxietyTest5} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="AnxietyTest6" component={AnxietyTest6} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest" component={DepressionTest} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest2" component={DepressionTest2} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest3" component={DepressionTest3} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest4" component={DepressionTest4} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest5" component={DepressionTest5} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest6" component={DepressionTest6} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest7" component={DepressionTest7} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="DepressionTest8" component={DepressionTest8} options={{
                headerShown: false,
            }} />
            <Stack.Screen name="MentalHCalculator" component={MentalHCalculator} options={{
                headerShown: false,
            }} />
        </Stack.Navigator>
    )
}