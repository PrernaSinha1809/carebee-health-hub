import React, { Component } from 'react';
import {
   SafeAreaView,
   Text,
   View,
   StyleSheet,
   Dimensions,
   ScrollView,
   FlatList,
   Animated,
   Alert,
   Button
} from 'react-native';
import { BarChart } from 'react-native-chart-kit';
import { TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import RNUrlPreview from 'react-native-url-preview';

var isHidden = true;
// ------------Function To Add Bar Graph-------------------//
const MyBarChart1week = () => {
   return (
      <>
         <BarChart
            data={{
               labels:
                  ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
               datasets: [
                  {
                     data: [35, 45, 28, 80, 99],
                  },
               ],
            }}
            width={Dimensions.get('window').width - 50}
            height={220}
            yAxisLabel={'₹'}
            chartConfig={{
               backgroundColor: 'red',
               backgroundGradientFrom: '#B8D5F9',
               backgroundGradientTo: 'white',
               decimalPlaces: 2,
               color: (opacity = 5) => `rgba(0, 76, 255, ${opacity})`,
               style: {
                  borderRadius: 16,
               },
            }}
            style={{
               marginVertical: 80,
               borderRadius: 20,
               // borderWidth:1,

            }}
         />
      </>
   );
};

const MyBarChart1month = () => {
   return (
      <>
         <BarChart
            data={{
               labels:
                  ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
               datasets: [
                  {
                     data: [35, 45, 28, 80, 99],
                  },
               ],
            }}
            width={Dimensions.get('window').width - 50}
            height={220}
            yAxisLabel={'₹'}
            chartConfig={{
               backgroundColor: 'red',
               backgroundGradientFrom: '#A4B8D2',
               backgroundGradientTo: 'white',
               decimalPlaces: 2,
               color: (opacity = 1) => `rgba(0, 76, 255, ${opacity})`,
               style: {
                  borderRadius: 16,
               },
            }}
            style={{
               marginVertical: 80,
               borderRadius: 20,
               // borderWidth:1,

            }}
         />
      </>
   );
};

const MyBarChart6month = () => {
   return (
      <>
         <BarChart
            data={{
               labels:
                  ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
               datasets: [
                  {
                     data: [35, 45, 28, 80, 99],
                  },
               ],
            }}
            width={Dimensions.get('window').width - 50}
            height={220}
            yAxisLabel={'₹'}
            chartConfig={{
               backgroundColor: 'red',
               backgroundGradientFrom: '#D7D9DB',
               backgroundGradientTo: 'white',
               decimalPlaces: 2,
               color: (opacity = 5) => `rgba(0, 76, 255, ${opacity})`,
               style: {
                  borderRadius: 16,
               },
            }}
            style={{
               marginVertical: 80,
               borderRadius: 20,
               // borderWidth:1,

            }}
         />
      </>
   );
};

class HeartRate extends Component {
   constructor(props) {
      super(props);
      this.onClick6Month = this.onClick6Month.bind(this);
      this.onClick1Month = this.onClick1Month.bind(this);
      this.onClickWeek = this.onClickWeek.bind(this);
      this.state = {
         active: '1m',
         bounceValue: new Animated.Value(100),  //This is the initial position of the subview
         buttonText: "Set Goals"
      };
      this.buttons = [
         { code: '1w', text: '1 Week', handler: this.onClickWeek },
         { code: '1m', text: '1 Month', handler: this.onClick1Month },
         { code: '6m', text: '6 Month', handler: this.onClick6Month },
      ];

   }

   // --------------------------------------------------------------------------------

   _toggleSubview() {
      this.setState({
         buttonText: !isHidden ? "Set Goals" : "Set Goals"
      });

      var toValue = 100;

      if (isHidden) {
         toValue = 0;
      }

      //This will animate the transalteY of the subview between 0 & 100 depending on its current state
      //100 comes from the style below, which is the height of the subview.
      Animated.spring(
         this.state.bounceValue,
         {
            toValue: toValue,
            velocity: 3,
            tension: 2,
            friction: 8,
            useNativeDriver: true
         }
      ).start();

      isHidden = !isHidden;
   }

   // --------------------------------------------------------------------------------

   // ------------Function Changing Color of Button-------------------//
   onClick6Month() {
      // console.log("clicked ");
      this.setState({ active: '6m' });
   }
   onClick1Month() {
      // console.log("clicked ");
      this.setState({ active: '1m' });

   }
   onClickWeek() {
      // console.log("clicked ");
      this.setState({ active: '1w' });
   }
   // ------------Function To Fix the Header on Top-------------------//
   fixedHeader = () => {
      var Sticky_header_View = (
         <View>
            <Text style={{ padding: 20, fontFamily: 'Poppins-Medium', textAlign: 'center', color: 'black', fontSize: 20 }}>Weight</Text>
         </View>
      );
      return Sticky_header_View;
   };

   getBtnStyle(which) {
      return which === this.state.active ? styles.btnActive : null;
   }

   // --------------------------------show the required graph-----------------------------------------------
   ShowHideTextComponentView = () => {

      if (this.state.status == true) {
         this.setState({ status: false })
      }
      else {
         this.setState({ status: true })
      }
   }

   render() {
      return (
         <View style={styles.container}>
            <View>
               <FlatList ListHeaderComponent={this.fixedHeader}
                  stickyHeaderIndices={[0]}
               />
            </View>
            <ScrollView style={{ marginBottom: 50 }}>
               <View>
                  <SafeAreaView>
                     <ScrollView>
                        <View style={styles.container}>
                           <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              {this.buttons.map(btn => (
                                 <View key={btn.code} style={[styles.btnInactive, this.getBtnStyle(btn.code)]}>
                                    <TouchableWithoutFeedback onPress={btn.handler}>
                                       <Text style={{ fontSize: 20 }}>{btn.text}</Text>
                                    </TouchableWithoutFeedback>
                                 </View>
                              ))}
                           </View>
                           {this.state.active == '1w' ? <View>
                              <MyBarChart1week />
                           </View> : null
                           }
                           {this.state.active == '1m' ? <View>
                              <MyBarChart1month />
                           </View> : null
                           }
                           {this.state.active == '6m' ? <View>
                              <MyBarChart6month />
                           </View> : null
                           }
                        </View>
                     </ScrollView>
                  </SafeAreaView>
               </View>
               <View>
                  <View style={{ flexDirection: 'row', width: '100%', height: 70, justifyContent: 'center', backgroundColor: '#D7DBD9' }}>
                     <TouchableOpacity style={styles.weightButton}>
                        <View style={{ width: 120, alignItems: 'center' }}>
                           <Text style={styles.buttonLabel}>Lose</Text>
                           <Text style={styles.buttonLabel}>23Kg</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.weightButton}>
                        <View style={{ width: 120, alignItems: 'center' }}>
                           <Text style={styles.buttonLabel}>Goal</Text>
                           <Text style={styles.buttonLabel}>55Kg</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.weightButton}>
                        <View style={{ width: 120, alignItems: 'center' }}>
                           <Text style={styles.buttonLabel}>Left</Text>
                           <Text style={styles.buttonLabel}>9Kg</Text>
                        </View>
                     </TouchableOpacity>
                  </View>
                  <View style={{ flexDirection: 'row', width: '100%', height: 70, justifyContent: 'center', backgroundColor: '#D7DBD9' }}>
                     <TouchableOpacity style={styles.weightButton}>
                        <View style={{ width: 120, alignItems: 'center' }}>
                           <Text style={styles.buttonLabel}>Completed</Text>
                           <Text style={styles.buttonLabel}>71.8%</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.weightButton}>
                        <View style={{ width: 120, alignItems: 'center' }}>
                           <Text style={styles.buttonLabel}>Goal till</Text>
                           <Text style={styles.buttonLabel}>13 July</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.weightButton} onPress={() => { this._toggleSubview() }}>
                        <View style={{ width: 100, height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 10, backgroundColor: '#72f9b5' }}>
                           {/* <Text style={styles.buttonLabel}>Set</Text>
                           <Text style={styles.buttonLabel}>Goal</Text> */}
                           <Text>{this.state.buttonText}</Text>
                        </View>
                     </TouchableOpacity>
                  </View>
               </View>
               <View style={{ padding: 10 }}>
                  <RNUrlPreview text={"any text to be parsed , https://lunginstitute.com/blog/low-blood-oxygen-affects-body/"} />
               </View>
               <Animated.View
                  style={[styles.subView,
                  { transform: [{ translateY: this.state.bounceValue }] }]}
                  
               >
                  <Text>This is a sub view</Text>
                  <Button
                     title="Press me"
                     onPress={() => Alert.alert('Simple Button pressed')}
                  />
               </Animated.View>
            </ScrollView>
         </View>
      );
   }
}

export default HeartRate

const styles = StyleSheet.create({
   container: {
      // flex: 1,
      // backgroundColor: '#fff',
      // alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10
   },
   btnActive: {
      backgroundColor: '#5096f1',
   },
   btnInactive: {
      backgroundColor: 'white',
      borderWidth: 1,
      borderRadius: 20,
      padding: 10,
   },
   weightButton: {
      width: '100%',
      height: 70,
      // flexDirection: 'row',
      // borderWidth: 1,
      justifyContent: 'center',
      // alignSelf: 'center'
   },
   button: {
      padding: 8,
    },
    buttonText: {
      fontSize: 17,
      color: "#007AFF"
    },
    subView: {
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: "#FFFFFF",
      height: 100,
    }
});