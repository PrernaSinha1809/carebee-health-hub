import React, { Component } from 'react';
import {
    SafeAreaView,
    Text,
    View,
    StyleSheet,
    Dimensions,
    ScrollView,
    FlatList
} from 'react-native';
import { BarChart } from 'react-native-chart-kit';
import { TouchableHighlight, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import RNUrlPreview from 'react-native-url-preview';

// ------------Function To Add Bar Graph-------------------//
const MyBarChart1week = () => {
    return (
        <>
            <BarChart
                data={{
                    labels:
                        ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
                    datasets: [
                        {
                            data: [35, 45, 28, 80, 99],
                        },
                    ],
                }}
                width={Dimensions.get('window').width - 50}
                height={220}
                yAxisLabel={'₹'}
                chartConfig={{
                    backgroundColor: 'red',
                    backgroundGradientFrom: '#B8D5F9',
                    backgroundGradientTo: 'white',
                    decimalPlaces: 2,
                    color: (opacity = 5) => `rgba(0, 76, 255, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                }}
                style={{
                    marginVertical: 80,
                    borderRadius: 20,
                    // borderWidth:1,

                }}
            />
        </>
    );
};

const MyBarChart1month = () => {
    return (
        <>
            <BarChart
                data={{
                    labels:
                        ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
                    datasets: [
                        {
                            data: [35, 45, 28, 80, 99],
                        },
                    ],
                }}
                width={Dimensions.get('window').width - 50}
                height={220}
                yAxisLabel={'₹'}
                chartConfig={{
                    backgroundColor: 'red',
                    backgroundGradientFrom: '#A4B8D2',
                    backgroundGradientTo: 'white',
                    decimalPlaces: 2,
                    color: (opacity = 1) => `rgba(0, 76, 255, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                }}
                style={{
                    marginVertical: 80,
                    borderRadius: 20,
                    // borderWidth:1,

                }}
            />
        </>
    );
};

const MyBarChart6month = () => {
    return (
        <>
            <BarChart
                data={{
                    labels:
                        ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
                    datasets: [
                        {
                            data: [35, 45, 28, 80, 99],
                        },
                    ],
                }}
                width={Dimensions.get('window').width - 50}
                height={220}
                yAxisLabel={'₹'}
                chartConfig={{
                    backgroundColor: 'red',
                    backgroundGradientFrom: '#D7D9DB',
                    backgroundGradientTo: 'white',
                    decimalPlaces: 2,
                    color: (opacity = 5) => `rgba(0, 76, 255, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                }}
                style={{
                    marginVertical: 80,
                    borderRadius: 20,
                    // borderWidth:1,

                }}
            />
        </>
    );
};

class BloodSugar extends Component {
    constructor(props) {
        super(props);
        this.onClick6Month = this.onClick6Month.bind(this);
        this.onClick1Month = this.onClick1Month.bind(this);
        this.onClickWeek = this.onClickWeek.bind(this);
        this.state = {
            active: '1m',
        };
        this.buttons = [
            { code: '1w', text: '1 Week', handler: this.onClickWeek },
            { code: '1m', text: '1 Month', handler: this.onClick1Month },
            { code: '6m', text: '6 Month', handler: this.onClick6Month },
        ];

    }

    // ------------Function Changing Color of Button-------------------//
    onClick6Month() {
        // console.log("clicked ");
        this.setState({ active: '6m' });
    }
    onClick1Month() {
        // console.log("clicked ");
        this.setState({ active: '1m' });

    }
    onClickWeek() {
        // console.log("clicked ");
        this.setState({ active: '1w' });
    }
    // ------------Function To Fix the Header on Top-------------------//
    fixedHeader = () => {
        var Sticky_header_View = (
            <View>
                <Text style={{ padding: 20, fontFamily: 'Poppins-Medium', textAlign: 'center', color: 'black', fontSize: 20 }}>Blood Sugar</Text>
            </View>
        );
        return Sticky_header_View;
    };

    getBtnStyle(which) {
        return which === this.state.active ? styles.btnActive : null;
    }

    // --------------------------------show the required graph-----------------------------------------------
    ShowHideTextComponentView = () => {

        if (this.state.status == true) {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <FlatList ListHeaderComponent={this.fixedHeader}
                        stickyHeaderIndices={[0]}
                    />
                </View>
                <View>
                    <SafeAreaView>
                        <ScrollView>
                            <View style={styles.container}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    {this.buttons.map(btn => (
                                        <View key={btn.code} style={[styles.btnInactive, this.getBtnStyle(btn.code)]}>
                                            <TouchableWithoutFeedback onPress={btn.handler}>
                                                <Text style={{ fontSize: 20 }}>{btn.text}</Text>
                                            </TouchableWithoutFeedback>
                                        </View>
                                    ))}
                                </View>
                                {this.state.active == '1w' ? <View>
                                    <MyBarChart1week />
                                </View> : null
                                }
                                {this.state.active == '1m' ? <View>
                                    <MyBarChart1month /> 
                                </View> : null
                                }
                                {this.state.active == '6m' ? <View>
                                    <MyBarChart6month /> 
                                </View> : null
                                }
                            </View>
                        </ScrollView>
                    </SafeAreaView>
                </View>
                <View style={{ padding: 10 }}>
                    <RNUrlPreview text={"any text to be parsed , https://lunginstitute.com/blog/low-blood-oxygen-affects-body/"} />
                </View>
            </View>
        );
    }
}

export default BloodSugar

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // backgroundColor: '#fff',
        // alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    btnActive: {
        backgroundColor: '#5096f1',
    },
    btnInactive: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 20,
        padding: 10,
    }
});