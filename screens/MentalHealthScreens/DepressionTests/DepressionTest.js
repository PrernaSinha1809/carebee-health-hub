import React from 'react';
import { Alert } from 'react-native';
import { StyleSheet, Text, View, Image, Touchable } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign'

export default class DepressionTest extends React.Component {
    constructor(props) {
        super(props);
        this.onClick1 = this.onClick1.bind(this)
        this.onClick2 = this.onClick2.bind(this)
        this.onClick3 = this.onClick3.bind(this)
        this.onClick4 = this.onClick4.bind(this)
        this.state = {
            action: '0',
            value: '0',
            score: '0'
        }
        this.buttons = [
            { code: '0', text: 'Not at all', backgroundColor: null, color: '#1672EC', handler: this.onClick1 },
            { code: '1', text: 'Few days', backgroundColor: null, color: '#1672EC', handler: this.onClick2 },
            { code: '2', text: 'Several days', backgroundColor: null, color: '#1672EC', handler: this.onClick3 },
            { code: '3', text: 'Nearly everyday', backgroundColor: null, color: '#1672EC', handler: this.onClick4 },

        ]
    }

    onClick1 = () => {
        this.setState({ action: '0', value: '0', score: '0' })
        this.buttons.map(btn => {
            if (btn.code == '0') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#1672EC',
                        btn.color = 'white'
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }else{
                if(btn.backgroundColor == '#1672EC'){
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }
        }
        )
        this.props.navigation.navigate('DepressionTest2', {value: '0'})
    }

    onClick2 = () => {
        this.setState({action: '1', value: '1', score: '1'})
        this.buttons.map(btn => {
            if (btn.code == '1') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#1672EC',
                        btn.color = 'white'
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }else{
                if(btn.backgroundColor == '#1672EC'){
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }
        }
        )
        this.props.navigation.navigate('DepressionTest2', {value: '1'})
    }

    onClick3 = () => {
        this.setState({action: '2', value: '2', score: '2'})
        this.buttons.map(btn => {
            if(btn.code == '2'){
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#1672EC',
                        btn.color = 'white'
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }else{
                if(btn.backgroundColor == '#1672EC'){
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }
        })
        this.props.navigation.navigate('DepressionTest2', {value: '2'})
    }

    onClick4 = () => {
        this.setState({action: '3', value: '3', score: '3'})
        this.buttons.map(btn => {
            if(btn.code == '3'){
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#1672EC',
                    btn.color = 'white'
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }else{
                if(btn.backgroundColor == '#1672EC'){
                    btn.backgroundColor = null,
                        btn.color = '#1672EC'
                }
            }
        })
        this.props.navigation.navigate('DepressionTest2', {value: '3'})
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.bgImageContainer}>
                    <Image source={require('../../../assets/icons/Ellipse49(1).png')} />
                </View>
                <View style={styles.bgImage2Container}>
                    <Image source={require('../../../assets/icons/Ellipse49(2).png')} />
                </View>
                <View style={styles.labelContainer}>
                    <Text style={styles.labelStyle}>Little interest or pleasure in doing things?</Text>
                </View>
                <View>
                    {this.buttons.map(index => (
                        <View key={index.code}>
                            <TouchableOpacity style={[styles.buttonStyle, { backgroundColor: index.backgroundColor }]} onPress={index.handler}>
                                <Text style={[styles.buttonLabelStyle, { color: index.color }]}>{index.text}</Text>
                            </TouchableOpacity>
                        </View>
                    ))}
                </View>
                <View style={styles.buttonViewContainer}>
                    <TouchableOpacity style={styles.circleButtonContainer} onPress={() => this.props.navigation.navigate('DepressionTest2')}>
                        <Icon name="right" size={20} color='#1672EC' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bgImageContainer: {
        position: 'absolute',
        top: 0,
        left: 0
    },
    bgImage2Container: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    labelStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        color: '#0A3977',
        textAlign: 'center'
    },
    labelContainer: {
        width: wp('80%'),
        position: 'absolute',
        top: hp('8%')
    },
    buttonStyle: {
        borderWidth: 3,
        width: wp('70%'),
        height: hp('8%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#1672EC',
        marginVertical: hp('2%')
    },
    buttonLabelStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 15,
        // color: '#1672EC'
    },
    circleButtonContainer: {
        width: wp('15%'),
        height: hp('8%'),
        borderRadius: 50,
        borderWidth: 3,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('-2%'),
        // paddingTop: hp('5%'),
        backgroundColor: 'white',
        elevation: 10,
        borderColor: '#C5DCFA'
    },
    buttonViewContainer: {
        position: 'absolute',
        bottom: hp('10%')
    }
});