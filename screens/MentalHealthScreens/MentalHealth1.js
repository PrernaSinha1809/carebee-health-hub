import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign'

export default class MentalHealth1 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={require('../../assets/icons/positive-thinking1.png')} />
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>Are you curious to know more about your mental well-being?</Text>
                </View>
                <View style={styles.text2Container}>
                    <Text style={styles.text2Style}>Do You Know?</Text>
                </View>
                <View style={styles.text3Container}>
                    <Text style={styles.text3Style}>MENTAL ILLNESS CAN SOMETIMES BE ASSOCIATED WITH CHRONIC PHYSICAL ILLNESS INCLUDING HEART DISEASE, HIGH BLOOD PRESSURE AND OBESITY.</Text>
                </View>
                <View style={styles.buttonViewContainer}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('MentalHealth3')}>
                        <Icon name="right" size={20} color='#009E8E' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textContainer: {
        margin: 30
    },
    textStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        textAlign: 'center',
        color: '#004F47'
    },
    text2Container: {
        marginVertical: 10
    },
    text2Style: {
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        color: '#0CDBC6'
    },
    text3Container: {
        marginVertical: 10,
        marginHorizontal: 60
    },
    text3Style: {
        fontFamily: 'Poppins-Regular',
        fontSize: 10,
        textAlign: 'center',
        color: '#A1A1A1'
    },
    // buttonViewContainer: {
    //     elevation: 10,
    //     borderWidth: 1
    // },
    buttonContainer: {
        width: 50,
        height: 50,
        borderRadius: 50,
        // borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        backgroundColor: '#ABFBF3',
        elevation: 10
    }
});