import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class MentalHealthSplash extends React.Component {
   constructor() {
      super();
      this.state = {
         isVisible: true,
      }
   }
   Hide_Splash_Screen = () => {
      this.setState({
         isVisible: false
      });
   }

   componentDidMount() {
      var that = this;
      setTimeout(function () {
         that.Hide_Splash_Screen();
      }, 3000);
   }

   render() {
      let Splash_Screen = (
         
         <View style={styles.SplashScreen_RootView}>
            <View style={{ position: 'absolute', right: 0, top: 0 }}>
               <Image source={require('../../assets/icons/Ellipse50.png')} />
            </View>
            <View style={{ position: 'absolute', right: 0, top: 0 }}>
               <Image source={require('../../assets/icons/Ellipse51.png')} />
            </View>
            <View style={{ position: 'absolute', right: 0, top: 0 }}>
               <Image source={require('../../assets/icons/Ellipse52.png')} />
            </View>
            <View style={{ position: 'absolute', right: 0, top: 0 }}>
               <Image source={require('../../assets/icons/Ellipse53.png')} />
            </View>
            <View style={styles.SplashScreen_ChildView}>
               <View style={styles.ellipse54Style}>
                  <Image source={require('../../assets/icons/Ellipse54.png')} />
               </View>
               <Text style={styles.splashText}>Health</Text>
            </View>
            <View style={{ position: 'absolute', left: 0, bottom: 0 }}>
               <Image source={require('../../assets/icons/Ellipse55.png')} />
            </View>
            <View style={{ position: 'absolute', left: 0, bottom: 0 }}>
               <Image source={require('../../assets/icons/Ellipse56.png')} />
            </View>
            <View style={{ position: 'absolute', left: 0, bottom: 0 }}>
               <Image source={require('../../assets/icons/Ellipse57.png')} />
            </View>
            <View style={{ position: 'absolute', left: 0, bottom: 0 }}>
               <Image source={require('../../assets/icons/Ellipse58.png')} />
            </View>
         </View>
         // </View>
      )
      return (
         <View style={styles.container}>

            {
               (this.state.isVisible === true) ? Splash_Screen : <View style={styles.parentStyle}>
                  <View style={styles.dumbellViewStyle}>
                     <Image style={styles.dumbellStyle}
                        source={require('../../assets/icons/dumbbell.png')}
                     />
                  </View>
                  <View style={styles.labelStyle}>
                     <Text style={styles.afterSplashTextStyle}> Physical</Text>
                     <Text style={styles.healthText}>Health?</Text>
                  </View>
                  <View style={styles.yesNoTouchableStyle}>
                     <TouchableOpacity style={styles.answerTouchableStyle} onPress={() => this.props.navigation.navigate('MentalHealthQuest')}>
                        <Text style={styles.yesTextStyle}>Yes</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.answerTouchableStyle}>
                        <Text style={styles.yesTextStyle}>No</Text>
                     </TouchableOpacity>
                  </View>
               </View>
            }
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      // alignItems: 'center',
      justifyContent: 'center',
   },
   SplashScreen_RootView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   SplashScreen_ChildView: {
      // justifyContent: 'center',
      // alignItems: 'center'
   },
   splashText: {
      fontFamily: 'Poppins-Regular',
      fontSize: 46,
      color: '#009E8E'
   },
   afterSplashTextStyle: {
      textAlign: 'center',
      fontFamily: 'Poppins-Medium',
      fontSize: 33,
      color: '#1672EC'
   },
   yesNoTouchableStyle: {
      flexDirection: 'row',
      width: '80%',
      height: 100,
      justifyContent: 'space-between',
      // borderWidth: 1
   },
   parentStyle: {
      width: '95%',
      // justifyContent: 'center',
      alignItems: 'center',
      // borderWidth: 1
   },
   answerTouchableStyle: {
      borderWidth: 3,
      borderRadius: 5,
      width: '120%',
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: '#1672EC'
   },
   yesTextStyle: {
      fontFamily: 'Poppins-Medium',
      fontSize: 23,
      margin: 40,
      color: '#1672EC'
   },
   healthText: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      textAlign: 'right',
      // borderWidth: 1, 
      width: '75%',
      color: '#1672EC',
      // marginBottom: 
      marginTop: -12
   },
   labelStyle: {
      // borderWidth: 1,
      height: 100,
      marginBottom: 80,
      width: '80%',
   },
   dumbellStyle: {
      height: 83,
      width: 83
   },
   dumbellViewStyle: {
      alignSelf: 'flex-end',
      position: 'absolute',
      right: 70,
      top: -40
   },
   ellipse54Style: {
      position: 'absolute',
      top: -10,
      // bottom: -20,
      right: -10,
      // borderWidth: 1
   }
});