import React from 'react';
import { Linking } from 'react-native';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Unorderedlist from 'react-native-unordered-list';
import Icon from 'react-native-vector-icons/AntDesign'

export default class MentalHealth3 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.text1Container}>
                    <Text style={styles.text1Style}>Most common types of Mental Health Problems:</Text>
                </View>
                <View style={styles.text2Container}>
                    <Unorderedlist><Text style={styles.text2Style}>Stress</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Anxiety</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Depression</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Sleep - Wake disorder (either you are sleep less or more)</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Sexual dysfuntions</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Internet Addiction</Text>
                    </Unorderedlist>
                    <Unorderedlist><Text style={styles.text2Style}>Substance Related Addiction</Text>
                    </Unorderedlist>
                </View>
                <View style={styles.text3Container}>
                    <Text style={styles.text3Style}>To know more on this, visit</Text>
                    <Text style={styles.textLink3Style} onPress={() => Linking.openURL('https://google.com')}>Resource Section</Text>
                </View>
                <View style={styles.buttonViewContainer}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('AssessmentTest')}>
                        <Icon name="right" size={20} color='#009E8E' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1Container: {
        margin: 30
    },
    text1Style: {
        fontFamily: 'Poppins-Medium',
        fontSize: 23,
        color: '#009E8E'
    },
    text2Container: {
        width: '80%'
    },
    text2Style: {
        // color: 'black'
        fontFamily: 'Poppins-Regular',
        fontSize: 16
    },
    text3Container: {
        margin: 30,
        alignSelf: 'flex-start'
    },
    text3Style: {
        fontFamily: 'Poppins-Regular',
        fontSize: 12,
    },
    textLink3Style: {
        color: '#009E8E'
    },
    buttonContainer: {
        width: 50,
        height: 50,
        borderRadius: 50,
        // borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        backgroundColor: '#ABFBF3',
        elevation: 10
    }
});