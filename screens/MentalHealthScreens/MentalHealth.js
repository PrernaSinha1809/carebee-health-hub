import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

export default class MentalHealthQuest extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.dumbellViewStyle}>
                    <Image style={styles.dumbellStyle}
                        source={require('../../assets/icons/brain.png')}
                    />
                </View>
                <View style={styles.labelStyle}>
                    <Text style={styles.afterSplashTextStyle}>Mental</Text>
                    <Text style={styles.healthText}>Health too?</Text>
                </View>
                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                <View style={styles.yesNoTouchableStyle}>
                    <TouchableOpacity style={styles.answerTouchableStyle} onPress={() => this.props.navigation.navigate('MentalHealth1')}>
                        <Text style={styles.yesTextStyle}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.answerTouchableStyle} onPress={() => this.props.navigation.navigate('MentalHealth2')}>
                        <Text style={styles.yesTextStyle}>No</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
     splashText: {
        fontFamily: 'Poppins-Regular',
        fontSize: 46,
        color: '#009E8E'
     },
     afterSplashTextStyle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 33,
        color: '#009E8E'
     },
     yesNoTouchableStyle: {
        flexDirection: 'row',
        width: '80%',
        height: 120,
        justifyContent: 'space-between',
        // borderWidth: 1
     },
     parentStyle: {
        width: '95%',
        // justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1
     },
     answerTouchableStyle: {
        borderWidth: 3,
        borderRadius: 5,
        width: '45%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#009E8E'
     },
     yesTextStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 23,
        margin: 40,
        color: '#009E8E'
     },
     healthText: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        textAlign: 'right',
        // borderWidth: 1, 
        width: '70%',
        color: '#0CDBC6',
        // marginBottom: 
        marginTop: -12
     },
     labelStyle: {
        // borderWidth: 1,
        height: 100,
        marginBottom: 80,
        width: '80%',
     },
     dumbellStyle: {
        height: 83,
        width: 83
     },
     dumbellViewStyle: {
        alignSelf: 'flex-end',
        position: 'absolute',
        right: 100,
        top: 110
     },
     ellipse54Style: {
        position: 'absolute',
        top: -10,
        // bottom: -20,
        right: -10,
        // borderWidth: 1
     }
});