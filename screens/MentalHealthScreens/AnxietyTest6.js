import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'

export default class AnxietyTest6 extends React.Component {
    constructor(props) {
        super(props);
        this.onClickFirst = this.onClickFirst.bind(this)
        this.onClickSecond = this.onClickSecond.bind(this)
        this.onClickThird = this.onClickThird.bind(this)
        this.onClickFourth = this.onClickFourth.bind(this)
        this.state = {
            active: '0',
            backgroundColor: null,
            color: 'white',
            score: '0',
            value: '0'
        };
        this.buttons = [
            { code: '0', text: 'Not at all', backgroundColor: null, color: '#009E8E', handler: this.onClickFirst },
            { code: '1', text: 'Few days', backgroundColor: null, color: '#009E8E', handler: this.onClickSecond },
            { code: '2', text: 'Several days', backgroundColor: null, color: '#009E8E', handler: this.onClickThird },
            { code: '3', text: 'Nearly everyday', backgroundColor: null, color: '#009E8E', handler: this.onClickFourth }
        ]

    }

    onClickFirst = () => {
        const { value } = this.props.route.params;
        this.setState({ action: '0', val: 0 }, () => {
            this.setState({ score: parseInt(value) + parseInt(this.state.val) }, () => {
                this.props.navigation.navigate('MentalHCalculator', { value: this.state.score })
            })
        })
        this.buttons.map(btn => {
            if (btn.code == '0') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#009E8E',
                        btn.color = 'white'
                    // this.setState({ value: '0' })
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                    // this.setState({ value: '0' })
                }
            }else{
                if(btn.backgroundColor == '#009E8E'){
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                }
            }
        })
    }

    onClickSecond = () => {
        const { value } = this.props.route.params;
        this.setState({ action: '1', val: 1 }, () => {
            this.setState({ score: parseInt(value) + parseInt(this.state.val) }, () => {
                this.props.navigation.navigate('MentalHCalculator', { value: this.state.score })
            })
        })
        this.buttons.map(btn => {
            if (btn.code == '1') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#009E8E',
                        btn.color = 'white'
                    // this.setState({ value: '0' })
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                    // this.setState({ value: '0' })
                }
            }else{
                if(btn.backgroundColor == '#009E8E'){
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                }
            }
        })
    }

    onClickThird = () => {
        const { value } = this.props.route.params;
        this.setState({ action: '2', val: 2 }, () => {
            this.setState({ score: parseInt(value) + parseInt(this.state.val) }, () => {
                this.props.navigation.navigate('MentalHCalculator', { value: this.state.score })
            })
        })
        this.buttons.map(btn => {
            if (btn.code == '2') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#009E8E',
                        btn.color = 'white'
                    // this.setState({ value: '0' })
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                    // this.setState({ value: '0' })
                }
            }else{
                if(btn.backgroundColor == '#009E8E'){
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                }
            }
        })
    }

    onClickFourth = () => {
        const { value } = this.props.route.params;
        this.setState({ action: '3', val: 3 }, () => {
            this.setState({ score: parseInt(value) + parseInt(this.state.val) }, () => {
                this.props.navigation.navigate('MentalHCalculator', { value: this.state.score })
            })
        })
        this.buttons.map(btn => {
            if (btn.code == '3') {
                if (btn.backgroundColor == null) {
                    btn.backgroundColor = '#009E8E',
                        btn.color = 'white'
                    // this.setState({ value: '0' })
                }
                else {
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                    // this.setState({ value: '0' })
                }
            }else{
                if(btn.backgroundColor == '#009E8E'){
                    btn.backgroundColor = null,
                        btn.color = '#009E8E'
                }
            }
        })
    }


    getButtonStyle = (code) => {
        // return code === this.state.active ? styles.button1Style : null;
        console.log(code)
    }

    render() {
        return (
            <View style={styles.container}>
                {/* -------------------------Background Images------------------------------- */}
                <View style={styles.bgImageStyle}>
                    <Image source={require('../../assets/icons/Rectangle-157.png')} />
                </View>
                <View style={styles.bgImage2Style}>
                    <Image source={require('../../assets/icons/Rectangle-157(1).png')} />
                </View>
                {/* -------------------------Buttons---------------------------------------- */}
                <View style={styles.labelContainer}>
                    <Text style={styles.labelStyle}>Feeling afraid as if something awful might happen?</Text>
                </View>

                {this.buttons.map(btn => (
                    <View key={btn.code} style={styles.buttonContainer}>
                        <TouchableOpacity onPress={btn.handler} style={[styles.button1Style, { backgroundColor: btn.backgroundColor }]}>
                            <Text style={[styles.buttonLabel, { color: btn.color }]}>{btn.text}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
                <View style={styles.buttonViewContainer}>
                    <TouchableOpacity style={styles.circleButtonContainer} onPress={() => this.props.navigation.navigate('MentalHealth3')}>
                        <Icon name="right" size={20} color='#009E8E' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bgImageStyle: {
        position: 'absolute',
        top: 0,
        left: 0
    },
    bgImage2Style: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    labelStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        textAlign: 'center',
        color: '#414141'
    },
    labelContainer: {
        margin: 30,
    },
    buttonContainer: {
        width: '90%',
        height: 70,
        // borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button1Style: {
        borderWidth: 3,
        width: '70%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
        borderColor: '#009E8E',
    },
    buttonLabel: {
        fontFamily: 'Poppins-Medium',
        fontSize: 15,
    },
    circleButtonContainer: {
        width: 50,
        height: 50,
        borderRadius: 50,
        borderWidth: 3,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        backgroundColor: 'white',
        elevation: 10,
        borderColor: '#ABFBF3'
    },
    activeButtonStyle: {
        backgroundColor: '#009E8E'
    }
});