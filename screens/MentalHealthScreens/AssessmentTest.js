import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class AssessmentTest extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={require('../../assets/icons/clock1.png')} />
                </View>
                <View style={styles.text1Container}>
                    <Text style={styles.text1Style}>Recognise your mental health symptoms within 2 mins</Text>
                </View>
                <View style={styles.text1Container}>
                    <Text style={styles.text2Style}>Let's take a <Text style={{color: '#414141'}}>quick 2 min</Text> assessment on Anxiety and Depression</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.button1Style} onPress={() => this.props.navigation.navigate('AnxietyTest')}>
                        <Text style={styles.buttonText1Style}>Anxiety</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.button2Style} onPress={() => this.props.navigation.navigate('DepressionTest')}>
                        <Text style={styles.buttonText2Style}>Depression</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text1Container: {
        margin: 30
    },
    text1Style: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        color: '#009E8E',
        textAlign: 'center'
    },
    text2Style: {
        fontFamily: 'Poppins-Regular',
        fontSize: 12,
        textAlign: 'center',
        color: '#818181'
    },
    button1Style: {
        backgroundColor: '#009E8E',
        width: 250,
        height: 49,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5,
        margin: 15
    },
    buttonText1Style: {
        color: 'white',
        fontFamily: 'Poppins-Medium',
        fontSize: 15
    },
    button2Style: {
        backgroundColor: '#1672EC',
        width: 250,
        height: 49,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5
    },
    buttonText2Style: {
        color: 'white',
        fontFamily: 'Poppins-Medium',
        fontSize: 15
    }
});