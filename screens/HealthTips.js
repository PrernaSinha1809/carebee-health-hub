import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import { Card } from 'react-native-elements';
import { FlatList, TouchableHighlight, TouchableWithoutFeedback } from 'react-native-gesture-handler';

class HealthTips extends Component {
    fixedHeader = () => {

        var Sticky_header_View = (
            <View>
                <Text style={{ padding: 20, fontFamily: 'Poppins-Medium', textAlign: 'center', color: 'black', fontSize: 20 }}> Health tips </Text>
            </View>
        );
        return Sticky_header_View;
    };
    render() {
        return (
            <View>
                <FlatList ListHeaderComponent={this.fixedHeader}
                    stickyHeaderIndices={[0]}
                />
                <ScrollView>
                    <SafeAreaView style={styles.container}>
                        <View style={styles.container}>
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Diabetes')}>
                                <Card title="Local Modules">
                                    <Text style={{ fontSize: 20, color: 'black', textAlign: 'center' }}>Diabetes</Text>
                                    <Image style={{ height: 100, width: '100%' }} source={require('../assets/images/diabetes.png')} />
                                    <Text style={styles.paragraph}>
                                        React Native Card View for Android and IOS using
                                        "react-native-elements"
                                    </Text>
                                </Card>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('MentalHealth')}>
                                <Card title="Local Modules">
                                    <Text style={{ fontSize: 20, color: 'black', textAlign: 'center' }}>Mental Health</Text>
                                    <Image style={{ height: 100, width: '100%' }} source={require('../assets/images/mental.png')} />
                                    <Text style={styles.paragraph}>
                                        React Native Card View for Android and IOS using
                                        "react-native-elements"
            </Text>
                                </Card>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('stress')}>
                                <Card title="Local Modules" >
                                    <Text style={{ fontSize: 20, color: 'black', textAlign: 'center' }}>Stress</Text>
                                    <Image style={{ height: 100, width: '100%' }} source={require('../assets/images/stress.png')}
                                    />
                                    <Text style={styles.paragraph}>
                                        React Native Card View for Android and IOS using
                                "react-native-elements"</Text>
                                </Card>
                            </TouchableWithoutFeedback>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        );
    }
}

export default HealthTips

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 40,
        backgroundColor: '#ecf0f1',
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#34495e',
    },
});