import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableHighlight, TextInput, TouchableOpacity, Keyboard, Alert } from 'react-native';
import { LinearTextGradient } from "react-native-text-gradient";
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Entypo';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { RadioButton } from 'react-native-paper';
import { Button } from 'react-native-elements';
import axios from 'axios';
import { widthPercentageToDP } from 'react-native-responsive-screen';

class OnBoarding3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Name: '',
            Password: '',
            Email: '',
            isExistingUser: false,
            clearInput: false,
            value : 'first'
        }

    }

    checkExistingUser = () => {
        // console.log('Hello');
        this.setState({ Name: '' })
        // console.log(this.state.Name)
        // console.log(this.state.userInfo);
        // this.setState({isExistingUser: true})
        // axios({
        //     method: 'POST',
        //     url: ' https://96a6629ac04d.ngrok.io/api/Carebee/findUser',
        //     data: this.state
        // }).then((response) => {
        //     if (response) {
        //         Alert.alert("A User with Username already exists. Choose a different Username")
        //         // this.setState({ Name: '', isExistingUser: false })
        //     }else{
        //         console.log('No such User found')
        //     }
        // }
        // )
        // Alert.alert("A User with Username " + response.data[0].Name + " already exists. Choose a different Username"))
        // .then()
        // .then((responseJson) => {
        //     // Alert.alert('User Already Exists. Please Create New User.', responseJson)
        //     console.log(responseJson);
        // })

        // .catch((error) => {
        //     console.error(error);
        // });

    }

    login = () => {
        // this.setState({isExistingUser: false})
        // console.log(this.state);
        // if (this.state.userInfo.Name == '' || this.state.userInfo.Password == '') {
        //     Alert.alert('Please fill all fields')
        // } else {
        //     axios({
        //         method: 'POST',
        //         url: 'https://d918ec00182d.ngrok.io/api/Carebee/findUser',
        //         data: this.state
        //     }).then((response) => Alert.alert("A User with Username " + response.data[0].Name + " already exists. Choose a different Username"))
        //         // Alert.alert("A User with Username " + response.data[0].Name + " already exists. Choose a different Username"))
        //         // .then()
        //         // .then((responseJson) => {
        //         //     // Alert.alert('User Already Exists. Please Create New User.', responseJson)
        //         //     console.log(responseJson);
        //         // })
        //         .catch((error) => {
        //             console.error(error);
        //         });
        // -------------------------Create New User-------------------Add 
        // axios({
        //     method: 'post',
        //     url: 'https://96a6629ac04d.ngrok.io/api/Carebee',
        //     data: this.state
        // })
        //     .then(res => console.log(res.data))
        //     .catch(err => console.log(err));
        // this.setState({
        //     Name: '',
        //     Email: '',
        //     Password: ''
        // });
        this.props.navigation.navigate("Homescreen");
        // }
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }} >
                    <LinearTextGradient
                        style={{ fontSize: 33 }}
                        locations={[0, 1]}
                        colors={["#1672ec", "#129a7f"]}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                    >
                        <Text style={styles.textTitle}>Carebee</Text>
                    </LinearTextGradient>
                    <Text style={styles.textSubHeading}>Consultation at your home</Text>
                    <View style={{ paddingVertical: 30 }}>
                        <View style={styles.PhoneNumberInputcontainer}>
                            <View style={styles.labelContainer}>
                                <Text style={styles.textLabelFont}>Username</Text>
                            </View>
                            <LinearGradient
                                colors={['#b3dafe', '#b3dafe', '#80aed9', '#80aed9', '#80aed9', '#a3ccf2']}
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
                                style={{ height: 60, alignItems: 'center', justifyContent: 'center', width: 270, padding: 5 }}
                            >
                                <View style={{ backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1, paddingHorizontal: 3 }}>
                                    {/* <View style={{ justifyContent: 'center' }}><Text styles={{ color: 'grey', fontFamily: 'Poppins-Regular', fontSize: 16 }}>+91 - </Text></View> */}
                                    <View>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            underlineColorAndroid="transparent"
                                            onChangeText={(Name) => { this.setState({ Name: Name }) }}
                                            onEndEditing={this.checkExistingUser}
                                            value={!this.state.clearInput ? this.state.Name : null}
                                        />
                                    </View>
                                    <TouchableOpacity style={styles.iconStyle} onPress={() => {
                                        this.setState({
                                            Name: '',
                                        })
                                    }}>
                                        <Icon name="circle-with-cross" size={20} color='black' />
                                    </TouchableOpacity>
                                </View>
                            </LinearGradient>
                        </View>

                    </View>
                    <View style={{ padding: 15 }}>
                        <View style={styles.PhoneNumberInputcontainer}>
                            <View style={styles.labelContainer}>
                                <Text style={styles.textLabelFont}>Password</Text>
                            </View>
                            <LinearGradient
                                colors={['#b3dafe', '#b3dafe', '#80aed9', '#80aed9', '#80aed9', '#a3ccf2']}
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
                                style={{ height: 60, alignItems: 'center', justifyContent: 'center', width: 270, padding: 5 }}
                            >
                                <View style={{ backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1, paddingHorizontal: 3 }}>
                                    {/* <View style={{ justifyContent: 'center' }}><Text styles={{ color: 'grey', fontFamily: 'Poppins-Regular', fontSize: 16 }}>+91 - </Text></View> */}
                                    <View>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            secureTextEntry={true}
                                            underlineColorAndroid="transparent"
                                            onChangeText={(Password) => { this.setState({ Password: Password }) }}
                                            value={!this.state.clearInput ? this.state.Password : null}
                                        />
                                    </View>
                                    <TouchableOpacity style={styles.iconStyle} onPress={() => {
                                        this.setState({
                                            Password: '',
                                        })
                                    }}>
                                        <Icon name="circle-with-cross" size={20} color='black' />
                                    </TouchableOpacity>
                                </View>
                            </LinearGradient>
                        </View>

                    </View>
                    <View style={{ padding: 15 }}>
                        <View style={styles.PhoneNumberInputcontainer}>
                            <View style={styles.labelContainer}>
                                <Text style={styles.textLabelFont}>E-mail(Optional)</Text>
                            </View>
                            <LinearGradient
                                colors={['#b3dafe', '#b3dafe', '#80aed9', '#80aed9', '#80aed9', '#a3ccf2']}
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
                                style={{ height: 60, alignItems: 'center', justifyContent: 'center', width: 270, padding: 5 }}
                            >
                                <View style={{ backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1, paddingHorizontal: 3 }}>
                                    {/* <View style={{ justifyContent: 'center' }}><Text styles={{ color: 'grey', fontFamily: 'Poppins-Regular', fontSize: 16 }}>+91 - </Text></View> */}
                                    <View>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            underlineColorAndroid="transparent"
                                            onChangeText={(Email) => { this.setState({ Email: Email }) }}
                                            value={!this.state.clearInput ? this.state.Email : null}
                                            keyboardType="email-address"
                                        />
                                    </View>
                                    <TouchableOpacity style={styles.iconStyle} onPress={() => {
                                        this.setState({
                                            Email: '',
                                        })
                                    }}>
                                        <Icon name="circle-with-cross" size={20} color='black' />
                                    </TouchableOpacity>
                                </View>
                            </LinearGradient>
                        </View>

                    </View>
                    <View style={styles.RadioButtonViewContainer}>
                        <View style={styles.RadioButtonContainer}>
                            <RadioButton
                                value='first'
                                status={this.state.value == 'first' ? 'checked' : 'unchecked'}
                                onPress={() => this.setState({ value: 'first' })}
                                color="#1672ec"
                            />
                            <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#1672ec', fontSize: 12 }}>Male</Text>
                        </View>
                        <View style={styles.RadioButtonContainer}>
                            <RadioButton
                                value='second'
                                status={this.state.value == 'second' ? 'checked' : 'unchecked'}
                                onPress={() => this.setState({ value: 'second' })}
                                color="#1672ec"
                            />
                            <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#1672ec', fontSize: 12 }}>Female</Text>
                        </View>
                        <View style={styles.RadioButtonContainer}>
                            <RadioButton
                                value='third'
                                status={this.state.value == 'third' ? 'checked' : 'unchecked'}
                                onPress={() => this.setState({ value: 'third' })}
                                color="#1672ec"
                            />
                            <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#1672ec', fontSize: 12 }}>Others</Text>
                        </View>
                    </View>
                    <View style={{ width: 150, borderRadius: 10 }}>
                        <Button
                            title="Proceed"
                            style={{ color: '#1672ec', borderRadius: 10 }}
                            onPress={() => this.login()}
                        />
                        {/* <Button
                            title="Proceed"
                            style={{ color: '#1672ec', borderRadius: 10 }}
                            onPress={() => this.login()}
                        /> */}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
export default OnBoarding3

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textTitle: {
        fontFamily: 'Poppins-Medium'
    },
    textSubHeading: {
        fontFamily: 'Poppins-Regular',
        fontSize: 15
    },
    arrowButton: {
        backgroundColor: "#1672ec",
        borderRadius: 100,
        justifyContent: 'center',
        padding: 10,
        width: 45,
        height: 45
    },
    labelContainer: {
        // fontFamily: 'Poppins-Medium',
        position: 'absolute',
        backgroundColor: '#FFF',
        top: -15,
        left: 25,
        padding: 5,
        zIndex: 50,
    },
    textLabelFont: {
        fontFamily: 'Poppins-Medium',
        textDecorationColor: '#7a7a7a',
        fontSize: 15
    },
    fcontainer: {
        height: 70,
        position: 'relative',
        // width: 200,
    },
    textInputStyle: {
        height: 52,
        width: 240,
        borderWidth: 10,
        borderColor: 'white',
        backgroundColor: 'white',
        fontSize: 16,
        fontFamily: 'Poppins-Medium',
        color: '#414141'
    },
    iconStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    RadioButtonViewContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 30,
        width: widthPercentageToDP('80'),
    },
    RadioButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: widthPercentageToDP('20'),
        // padding: 5 ,
        // borderWidth: 1,
        alignItems: 'center'
    }
});
