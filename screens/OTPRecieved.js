import React, { Component } from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native'
import OtpAsset from '../assets/fonts/OTP_asset.svg';
import { Button } from 'react-native-elements';
import OTPInput from './Components/app';
class OTPrecieved extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 100 }}>
                    <View>
                        <OtpAsset />
                    </View>
                    <View>
                        <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 15 }}>Enter the OTP sent in your phone</Text>
                    </View>
                    <View>
                        <OTPInput />
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 30 }}>
                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12 }}>Didn't recieve the code?</Text>
                    <Text style={{ color: "#1672ec", fontFamily: 'Poppins-Regular', fontSize: 12 }}
                    // onPress={goToResendOTP}
                    > Resend OTP</Text>
                </View>
                <View style={{ width: 150, borderRadius: 10 }}>
                    <Button
                        title="Proceed"
                        style={{ color: '#1672ec', borderRadius: 10 }}
                        onPress={() => this.props.navigation.navigate('OnBoarding1')}
                    />
                </View>
            </View>
        );
    }
}

export default OTPrecieved

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});