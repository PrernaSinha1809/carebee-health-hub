import React, { Component } from 'react'
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native'
import OnBoarding_2Asset from '../assets/fonts/Onboarding_2.svg'
import Icon from 'react-native-vector-icons/Ionicons';

class OnBoarding2 extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <View>
                        <OnBoarding_2Asset />
                    </View>
                    <View style={{ paddingVertical: 20 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 23, color: '#1672ec' }}>Recieve remainders</Text>
                    </View>
                    <View style={{ paddingBottom: 20, padding: 10 }}>
                        <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 13, textAlign: 'center' }}>You will recieve notifications for taking medicines and upcoming appointments</Text>
                    </View>
                    <View style={{ flexDirection: 'row', width: 300, justifyContent: 'space-between', paddingVertical: 20 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 15, color: '#009e8e' }}
                                onPress={() => this.props.navigation.navigate('OnBoarding3')}
                            >Skip</Text>
                        </View>
                        <View style={{ alignContent: 'flex-end' }}>
                            <TouchableOpacity style={{ backgroundColor: "#abfbf3", borderRadius: 100, justifyContent: 'center', padding: 10, width: 45, height: 45 }}
                                onPress={() => this.props.navigation.navigate('OnBoarding3')}
                            >
                                <Icon name="arrow-forward-outline" size={20} color='#009e8e' />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
export default OnBoarding2
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});