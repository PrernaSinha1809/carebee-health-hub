import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { StyleSheet, Text, View, Image, TextInput, ScrollView } from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5'
import OTPInput from './Components/app';

class ForgetPassword extends Component {
   constructor(props) {
      super(props);
      this.state = {
         show1: true,
         show2: false
      }
   }
   render() {
      return (
         <View style={styles.container}>
            <View style={styles.headerTab}>
               <TouchableOpacity onPress={() => this.props.navigation.navigate('OpenPage')}>
                  <Icon name="arrow-left" size={20} color='black' />
               </TouchableOpacity>
            </View>
            <View style={{ position: 'absolute', bottom: 0}}>
               <ScrollView contentContainerStyle={{height: heightPercentageToDP('90'), 
            width: widthPercentageToDP('90'), borderWidth: 1}}>

               </ScrollView>
               {/* <ScrollView contentContainerStyle={styles.scrollContainer}>

               </ScrollView> */}
            </View>
         </View>
      );
   }
}

export default ForgetPassword

const styles = StyleSheet.create({
   container: {
      // borderColor: 'blue',
      // borderWidth: 10,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   scrollContainer: {
      // borderWidth: 10,
      height: heightPercentageToDP('100'),
      width: widthPercentageToDP('100'),
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
   },
   imageContainer: {
      // borderWidth: 1,
      width: widthPercentageToDP('80'),
      height: heightPercentageToDP('40'),
      // justifyContent: 'center',
      alignItems: 'center',
      // position: 'absolute',
      bottom: heightPercentageToDP('10')
   },
   imageStyle: {
      width: widthPercentageToDP('60'),
      height: heightPercentageToDP('40'),
      resizeMode: 'contain'
   },
   imageStyle2: {
      resizeMode: 'contain',
      width: widthPercentageToDP('40'),
      height: heightPercentageToDP('40'),
   },
   labelStyle: {
      fontFamily: 'Poppins-Medium',
      fontSize: 23,
      textAlign: 'center'
   },
   labelContainer: {
      // borderWidth: 1,
      marginVertical: heightPercentageToDP('2')
   },
   emailLabel: {
      fontFamily: 'Poppins-Regular',
      fontSize: 15
   },
   textInput: {
      borderBottomWidth: 1,
      fontFamily: 'Poppins-Regular',
      fontSize: 13
   },
   mainContainer: {
      bottom: heightPercentageToDP('10'),
      width: widthPercentageToDP('80'),
   },
   buttonConatiner: {
      // borderWidth: 1,
      height: heightPercentageToDP('5'),
      width: widthPercentageToDP('40'),
      // alignItems: 'center'
      // justifyContent: 'center'
      // position: 'absolute',
      // bottom: heightPercentageToDP('15')
   },
   button: {
      // borderWidth: 1,
      height: heightPercentageToDP('7'),
      width: widthPercentageToDP('40'),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#1672ec',
      borderRadius: 10,
      elevation: 10
   },
   buttonLabel: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
      color: 'white'
   },
   headerTab: {
      position: 'absolute',
      top: 20,
      left: 20
   },
   inputContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      // marginVertical: heightPercentageToDP('10')
      paddingVertical: heightPercentageToDP('5')
   }
});