import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { TouchableHighlight, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import Blood_OxygenIcon from '../assets/fonts/spo2.svg'
import Blood_SugarIcon from '../assets/fonts/blood_sugar.svg'
import Heart_RateIcon from '../assets/fonts/heart_rate.svg'
import Body_weightIcon from '../assets/fonts/body_weight.svg'

class Dashboard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ marginTop: 20, width: '80%' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20, color: '#414141' }}>Dashboard</Text>
                        </View>
                        <View>
                            <View style={{ borderWidth: 1, borderRadius: 100, height: 60, width: 60, justifyContent: 'center', alignItems: 'center' }}>
                                <Image style={{ height: 50, width: 50 }} source={require('../assets/profile/person.png')} />
                            </View>
                        </View>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 20 }}>
                            <View>
                                <TouchableWithoutFeedback
                                    onPress={() => this.props.navigation.navigate('Blood_Oxygen')}
                                    style={{ backgroundColor: '#8ab9f6', height: 220, width: 140, borderRadius: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, padding: 10 }}>
                                        <View style={{ width: '70%' }}>
                                            <Text style={{ fontSize: 16, color: '#0f56b3', textAlign: 'left', fontFamily: 'Poppins-Medium' }}>BLOOD OXYGEN</Text>
                                        </View>
                                        <View style={{}}>
                                            <Icon name="arrow-forward" size={20} color='#0f56b3' />
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                        <Blood_OxygenIcon width={80} height={80} />
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingBottom: 10 }}>
                                        <View>
                                            <Text style={{ color: '#0a3977', fontFamily: 'Poppins-Medium', fontSize: 20 }}>98%</Text>
                                        </View>
                                        <View>
                                            <Text style={{ color: '#0f56b3', fontSize: 13, fontFamily: 'Poppins-Regular' }}>Updated 1 day ago</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View>
                                <TouchableWithoutFeedback
                                    onPress={() => this.props.navigation.navigate('Blood_Sugar')}
                                    style={{ backgroundColor: '#f584ad', height: 220, width: 140, borderRadius: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, padding: 10 }}>
                                        <View style={{ width: '70%' }}>
                                            <Text style={{ fontSize: 16, color: '#a90e46', textAlign: 'left', fontFamily: 'Poppins-Medium' }}>BLOOD SUGAR</Text>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon name="arrow-forward" size={20} color='#a90e46' />
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                        <Blood_SugarIcon width={80} height={80} />
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingBottom: 10 }}>
                                        <View>
                                            <Text style={{ color: '#70092f', fontFamily: 'Poppins-Medium', fontSize: 20 }}>98%</Text>
                                        </View>
                                        <View>
                                            <Text style={{ color: '#a90e46', fontSize: 13, fontFamily: 'Poppins-Regular' }}>Updated 1 day ago</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 20 }}>
                            <View>
                                <TouchableWithoutFeedback
                                    onPress={() => this.props.navigation.navigate('Heart_Rate')}
                                    style={{ backgroundColor: '#fcd0cf', height: 220, width: 140, borderRadius: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, padding: 10 }}>
                                        <View style={{ width: '70%' }}>
                                            <Text style={{ fontSize: 16, color: '#da100b', textAlign: 'left', fontFamily: 'Poppins-Medium' }}>HEART RATE</Text>
                                        </View>
                                        <View style={{}}>
                                            <Icon name="arrow-forward" size={20} color='#da100b' />
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                        <Heart_RateIcon width={80} height={80} />
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingBottom: 10 }}>
                                        <View>
                                            <Text style={{ color: '#910b08', fontFamily: 'Poppins-Medium', fontSize: 20 }}>98%</Text>
                                        </View>
                                        <View>
                                            <Text style={{ color: '#da100b', fontSize: 13, fontFamily: 'Poppins-Regular' }}>Updated 1 day ago</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <View>
                                <TouchableWithoutFeedback
                                    onPress={() => this.props.navigation.navigate('Weight')}
                                    style={{ backgroundColor: '#8ce590', height: 220, width: 140, borderRadius: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, padding: 10 }}>
                                        <View style={{ width: '70%' }}>
                                            <Text style={{ fontSize: 16, color: '#155d18', textAlign: 'left', fontFamily: 'Poppins-Medium' }}>WEIGHT</Text>
                                        </View>
                                        <View style={{}}>
                                            <Icon name="arrow-forward" size={20} color='#155d18' />
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                        <Body_weightIcon width={80} height={80} />
                                    </View>
                                    <View style={{ paddingLeft: 10, paddingBottom: 10 }}>
                                        <View>
                                            <Text style={{ color: '#155d18', fontFamily: 'Poppins-Medium', fontSize: 20 }}>98%</Text>
                                        </View>
                                        <View>
                                            <Text style={{ color: '#155d18', fontSize: 13, fontFamily: 'Poppins-Regular' }}>Updated 1 day ago</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default Dashboard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        // justifyContent: 'center',
        borderWidth: 1
    },
});