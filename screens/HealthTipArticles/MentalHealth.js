import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

class MentalHealth extends Component {
   render() {
      return (
         <View style = {styles.container}>
             <Image style={{height: 100, width: '100%'}} source={require('../../assets/images/mental.png')}/>
            <Text>Open up App.js to start working on your app!</Text>
            <Text>Changes you make will automatically reload.</Text>
            <Text>Shake your phone to open the developer menu.</Text>
         </View>
      );
   }
}

export default MentalHealth

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
   },
});