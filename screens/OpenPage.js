import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, TextInput, TouchableOpacity, Keyboard, Alert } from 'react-native';
import { LinearTextGradient } from "react-native-text-gradient";
import HomeIcon from '../assets/fonts/carebee_opening_asset.svg';
import CheckBox from '@react-native-community/checkbox';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


export default class OpenPage extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         phoneNumber: ''
      }
   }

   handlePhoneNumber = () => {
      console.log("-----",this.state.phoneNumber);
      fetch('http://192.168.1.13:3000/login', {
         method: 'get',
         data: this.state.phoneNumber
      }).then(response => console.log(response));
   }
   render() {
      return (
         <View style={styles.container}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ justifyContent: 'center', alignItems: 'center', padding: 30 }} >
               <LinearTextGradient
                  style={{ fontSize: 33 }}
                  locations={[0, 1]}
                  colors={["#1672ec", "#129a7f"]}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
               >
                  <Text style={styles.textTitle}>Carebee</Text>
               </LinearTextGradient>
               <Text style={styles.textSubHeading}>Consultation at your home</Text>
               <View style={{ padding: 10 }}>
                  <HomeIcon />
               </View>
               <View style={{ padding: 15 }}>
                  <View style={styles.PhoneNumberInputcontainer}>
                     <View style={styles.labelContainer}>
                        <Text style={styles.textLabelFont}>Phone number</Text>
                     </View>
                     <LinearGradient
                        colors={['#b3dafe', '#b3dafe', '#80aed9', '#80aed9', '#80aed9', '#a3ccf2']}
                        start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
                        style={{ height: 60, alignItems: 'center', justifyContent: 'center', width: 270, padding: 5 }}
                     >
                        <View style={{ backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1, paddingHorizontal: 3 }}>
                           <View style={{ justifyContent: 'center' }}><Text styles={{ color: 'grey', fontFamily: 'Poppins-Regular', fontSize: 16 }}>+91 - </Text></View>
                           <View>
                              <TextInput
                                 style={{ height: 50, width: 175, borderWidth: 3, borderColor: 'white', backgroundColor: 'white' }}
                                 keyboardType='numeric'
                                 maxLength={10}
                                 ref={input => { this.textInput = input }}
                                 value={this.state.phoneNumber}
                                 onChangeText={(value) => this.setState({ phoneNumber: value })}
                                 // {PhoneNumberInput => this.setState({ PhoneNumberInput })}
                                 underlineColorAndroid="transparent"
                              />
                           </View>
                           <View style={{ justifyContent: 'center' }}>
                              <TouchableOpacity style={styles.arrowButton}
                                 onPress=
                                 // {this.handlePhoneNumber}
                              {() => this.props.navigation.navigate('OTPRecieved')}
                              >
                                 <Icon name="arrow-forward-outline" size={20} color='white' />
                              </TouchableOpacity>
                           </View>
                        </View>
                     </LinearGradient>
                  </View>
                  <TouchableHighlight >
                     <View>
                        <Text style={{ fontFamily: 'Poppins-Regular', textAlign: 'right', fontSize: 13, padding: 5 }}
                           onPress={() => this.props.navigation.navigate('ForgetPassword')}
                        >Forgot Password?</Text>
                     </View>
                  </TouchableHighlight>
               </View>
               <View style={{ flexDirection: 'row' }}>
                  <CheckBox
                     // value={isSelected}
                     // onValueChange={setSelection}
                     // style={styles.checkbox}
                     tintColors={{ true: '#1672ec' }}
                  />
                  <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, padding: 7 }}>I accept to terms and conditions</Text>
               </View>
            </TouchableWithoutFeedback>
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
   },
   textTitle: {
      fontFamily: 'Poppins-Medium'
   },
   textSubHeading: {
      fontFamily: 'Poppins-Regular',
      fontSize: 15
   },
   arrowButton: {
      backgroundColor: "#1672ec",
      borderRadius: 100,
      justifyContent: 'center',
      padding: 10,
      width: 45,
      height: 45
   },
   labelContainer: {
      // fontFamily: 'Poppins-Medium',
      position: 'absolute',
      backgroundColor: '#FFF',
      top: -15,
      left: 25,
      padding: 5,
      zIndex: 50,
   },
   textLabelFont: {
      fontFamily: 'Poppins-Medium',
      textDecorationColor: '#7a7a7a',
      fontSize: 15
   },
   fcontainer: {
      height: 70,
      position: 'relative',
      // width: 200,
   },
});