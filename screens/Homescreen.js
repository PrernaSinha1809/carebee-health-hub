import { View } from 'native-base';
import React from 'react';
import { Alert } from 'react-native';
import { StyleSheet, Text, SafeAreaView, ScrollView, StatusBar, Image, Dimensions } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Entypo';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { abs } from 'react-native-reanimated';


const { width } = Dimensions.get('window');


export default class Homescreen extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         searchedText: '',
         showview: false,
         showview2: false,
         parentOpacity: 1,
         // childOpacity: 1,
         // op: 0
      }
   }

   change = () => {
      this.setState({ parentOpacity: 0.2 })
   }
   updateText = () => {
      // this.setState({ searchedText: text })
      if (this.state.searchedText == '') {
         Alert.alert('Enter something to search!')
      } else {
         console.log(this.state.searchedText)
         this.setState({ searchedText: '' })
      }
   }

   // componentDidMount = () =>{
   //    this.props.navigation.addListener(
   //       'refresh',
   //       payload => {
   //          this.forceUpdate()
   //       }
   //    )
   // }
   render() {
      // const { isFocused } = this.props;
      return (
         <View style={styles.parentContainer}>
            {/* <View style={[styles.view1, {opacity: this.state.parentOpacity}]}> */}
            <View style={[styles.view1, { opacity: this.state.parentOpacity }]}>
               <View style={styles.headerContainer}>
                  <View style={styles.menuIconContainer}>
                     <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name="menu" size={25} />
                     </TouchableOpacity>
                  </View>
                  <View style={styles.notifyIconContainer}>
                     <TouchableOpacity onPress={() => this.setState({ parentOpacity: 0.5, showview: true })}>
                        <Icon1 name="notifications-outline" size={25} />
                     </TouchableOpacity>
                  </View>
               </View>
               <View style={styles.searchBarContainer}>
                  <TextInput
                     placeholder="Symptom, disease, doctors"
                     onChangeText={(text) => { this.setState({ searchedText: text }) }}
                     value={this.state.searchedText}
                  />
                  <TouchableOpacity onPress={this.updateText}>
                     <Icon1 name="search" />
                  </TouchableOpacity>
               </View>
               <View style={styles.topViewContainer}>
                  <View style={styles.topViewContainer2}>
                     <View>
                        <Text style={{ fontSize: 25, color: '#00776A', fontFamily: 'Poppins-SemiBold' }}>Find right doctor,</Text>
                        <Text style={{ fontSize: 25, color: '#00776A', fontFamily: 'Poppins-SemiBold' }}>Right now!</Text>
                     </View>
                     <View>
                        <Image source={require('../assets/icons/Layer2.png')} />
                     </View>
                  </View>
                  <View style={styles.findNowButtonContainer}>
                     <TouchableOpacity style={styles.findNowButton}
                     // onPress={this.updater.enqueueForceUpdate(this, callback, 'forceUpdate') }
                     >
                        <Text style={styles.findNowLabel}>Find Now</Text>
                     </TouchableOpacity>
                  </View>
               </View>
               <SafeAreaView style={styles.container}>
                  <ScrollView style={styles.scrollView}>
                     <View style={styles.card1Container}>
                        <View style={styles.heartImageContainer}>
                           <Image source={require('../assets/icons/Vector(1).png')} />
                        </View>
                        <View style={styles.card1LabelContainer}>
                           <Text style={styles.card1Label}>Don't forget to add your daily vitals</Text>
                        </View>
                        <View style={styles.addButtonContainer}>
                           <TouchableOpacity onPress={() => this.props.navigation.navigate('Vitals')}>
                              <Text style={styles.addButtonLabel}>+ Add</Text>
                           </TouchableOpacity>
                        </View>
                     </View>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 20 }}>
                        <View style={styles.card2Container}>
                           <View style={styles.heartImageContainer}>
                              <Image source={require('../assets/icons/Group181.png')} />
                           </View>
                           <View >
                              <Text style={styles.card2Label}>Good Morning.</Text>
                              <Text style={styles.card2Label}>How are you feeling?</Text>
                           </View>
                        </View>
                        <View style={styles.card2Container}>
                           <View style={styles.heartImageContainer}>
                              <Image source={require('../assets/icons/cross.png')} />
                           </View>
                           <View>
                              <TouchableOpacity onPress={() => this.props.navigation.navigate()}>
                                 <View style={{ width: '80%' }}>
                                    {/* <Text>Good Morning.</Text> */}

                                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 13, color: '#004F47' }}>Did you take a fruit today?</Text>
                                 </View>
                              </TouchableOpacity>
                           </View>
                        </View>
                     </View>
                     <View>
                        <Text style={styles.label3Style}>Mental Assessment</Text>
                     </View>
                     <View style={styles.card4Container}>
                        <View>
                           <View style={styles.ellipseImageContainer}>
                              <Image source={require('../assets/icons/Ellipse-50.png')} />
                           </View>
                           <View style={styles.ellipse2ImageContainer}>
                              <Image source={require('../assets/icons/Ellipse-49.png')} />
                           </View>
                        </View>
                        <View style={{ padding: 10 }}>
                           <View>
                              <Text style={styles.card4Label}>Little interest or pleasure in doing things?</Text>
                           </View>
                           <View>
                              <TouchableOpacity style={styles.arrowButton}>
                                 <Icon name="arrow-with-circle-right" size={25} color="#0F56B3" />
                              </TouchableOpacity>
                           </View>
                        </View>
                     </View>
                     <View style={styles.card4Container}>
                        <View>
                           <View style={styles.ellipseImageContainer}>
                              <Image source={require('../assets/icons/Rectangle160.png')} />
                           </View>
                           <View style={styles.ellipse2ImageContainer}>
                              <Image source={require('../assets/icons/Rectangle159.png')} />
                           </View>
                        </View>
                        <View style={{ padding: 10 }}>
                           <View>
                              <Text style={styles.card4Label}>Frequently feeling nervous or anxious or on edge?</Text>
                           </View>
                           <View>
                              <TouchableOpacity style={styles.arrowButton}>
                                 <Icon name="arrow-with-circle-right" size={25} color="#0F56B3" />
                              </TouchableOpacity>
                           </View>
                        </View>
                     </View>
                     <View>
                        <Text style={styles.label3Style}>Health Tips</Text>
                     </View>
                     <View>
                        <ScrollView
                           horizontal={true}
                           decelerationRate={0}
                        // snapToInterval={width - 40}
                        >
                           <View style={styles.view}>
                              <View style={{ marginTop: 10 }}>
                                 <Image source={require('../assets/icons/Rectangle164.png')} />
                              </View>
                              <View style={{ width: wp('30%') }}>
                                 <Text style={styles.healthTipsStyle}>Exercise</Text>
                              </View>
                              <View style={{ width: wp('30%') }}>
                                 <Text style={styles.healthTipsStyle2}>How Yoga help you to fight anxiety</Text>
                              </View>
                           </View>
                           <View style={styles.view}>
                              <View style={{ marginTop: 10 }}>
                                 <Image source={require('../assets/icons/Rectangle-164-(1).png')} />
                              </View>
                              <View style={{ width: wp('30%') }}>
                                 <Text style={styles.healthTipsStyle}>Food</Text>
                              </View>
                              <View style={{ width: wp('30%') }}>
                                 <Text style={styles.healthTipsStyle2}>5 magical food to get you ina good mood</Text>
                              </View>
                           </View>
                           <View style={styles.view} />
                           <View style={styles.view} />
                           <View style={styles.view} />
                        </ScrollView>
                     </View>
                  </ScrollView>
               </SafeAreaView>
            </View>
            <View>
               {this.state.showview == true ?
                  [(this.state.showview2 == false ?
                     <View key={0}>
                        <ScrollView contentContainerStyle={styles.tutorialView}>
                           <View style={styles.arrow1IconContainer}>
                              <Icon2 name="arrow-long-left" size={100} color="#777777" style={{
                                 // shadowOpacity: 2,
                                 textShadowRadius: 10,
                                 textShadowOffset: { width: 4, height: 4 }
                              }} />
                              <Text style={[styles.tutorialLabels, {
                                 textShadowRadius: 10,
                                 textShadowOffset: { width: 4, height: 4 }
                              }]}>Burger menu</Text>
                           </View>
                           <View style={{ borderWidth: 1, marginBottom: 40, elevation: 10 }}>
                              <Image style={styles.burgerMenu} source={require('../assets/icons/BurgerMenu.png')} />
                           </View>
                           <TouchableOpacity style={styles.okayButtonConatiner} onPress={() => this.setState({ showview2: true })}>
                              <Text style={[styles.tutorialLabels, { color: 'white' }]}>Okay</Text>
                           </TouchableOpacity>
                        </ScrollView>
                     </View> :
                     <View key={0}>
                        <ScrollView contentContainerStyle={styles.tutorialView}>
                           <View style={styles.arrow2IconContainer}>
                              <Icon2 name="arrow-long-down" size={100} color="#777777" style={{
                                 // shadowOpacity: 2,
                                 textShadowRadius: 10,
                                 textShadowOffset: { width: 4, height: 4 }
                              }} />
                              <Text style={[styles.tutorialLabels, {
                                 textShadowRadius: 10,
                                 textShadowOffset: { width: 4, height: 4 }
                              }]}>Home Tab</Text>
                           </View>
                           <View style={{ width: wp('50'), height: hp('25'), position: 'absolute' }}>
                              <TouchableOpacity style={[styles.okayButtonConatiner, { right: wp('22') }]}
                                 onPress={() => this.setState({ showview: false, parentOpacity: 1, showview2: false })}>
                                 <Text style={[styles.tutorialLabels, { color: 'white' }]}>Okay</Text>
                              </TouchableOpacity>
                           </View>
                           <View style={{ borderWidth: 1, bottom: 80, elevation: 10, left: wp('25') }}>
                              <Image style={styles.homeScreenMenu} source={require('../assets/icons/Homescreen.png')} />
                           </View>
                        </ScrollView>
                     </View>
                  )
                  ]
                  : null}

            </View>
         </View >
      );
   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingTop: StatusBar.currentHeight,
      paddingBottom: 70
   },
   scrollView: {
      // backgroundColor: 'pink',
      marginHorizontal: 20,
   },
   text: {
      fontSize: 42,
   },
   parentContainer: {
      flex: 1,
      backgroundColor: '#fff',
      // alignItems: 'center',
      // justifyContent: 'center',
      // paddingBottom: 50,
      // alignSelf: 'center'
      // flexDirection: 'row'
      // paddingHorizontal: 20
   },
   view1: {
      // borderWidth: 10,
      // flex: 1,
      height: hp('100'),
      width: wp('100'),
      alignItems: 'center',
      backgroundColor: 'white',
      position: 'absolute'
      // justifyContent: 'center',
   },
   headerContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: wp('94%'),
      paddingVertical: 20,
      // borderWidth: 1,
      // position: 'absolute',
      // marginTop: 10

   },
   searchBarContainer: {
      width: '90%',
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      // borderWidth: 1
      // position: 'absolute',
   },
   topViewContainer: {
      // borderWidth: 1,
      width: '90%',
      backgroundColor: '#ABFBF3',
      borderRadius: 20,
      padding: 10,
      elevation: 5
      // justifyContent: 'space-between'
   },
   topViewContainer2: {
      flexDirection: 'row',
      // borderWidth: 1,
      justifyContent: 'space-between'
   },
   findNowButtonContainer: {

   },
   findNowButton: {
      width: '50%',
      bottom: 20,
      justifyContent: 'center',
      alignItems: 'center',
      height: 40,
      borderRadius: 5,
      backgroundColor: '#009E8E',
      elevation: 5
   },
   findNowLabel: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
      color: 'white'
   },
   card1Container: {
      height: 100,
      borderRadius: 10,
      elevation: 5,
      backgroundColor: '#F1F1F1',
      marginHorizontal: 5
   },
   heartImageContainer: {
      position: 'absolute',
      top: 0,
      left: 0
   },
   card1Label: {
      fontFamily: 'Poppins-Medium',
      fontSize: 16,
      color: '#910B08',
   },
   card1LabelContainer: {
      width: '50%',
      position: 'absolute',
      top: 10,
      left: 30
   },
   addButtonContainer: {
      position: 'absolute',
      right: 40,
      bottom: 40,
      width: '25%',
      height: 40,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FCD0CF',
      elevation: 5
   },
   addButtonLabel: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
      color: '#DA100B'
   },
   card2Container: {
      height: 100,
      borderRadius: 10,
      elevation: 5,
      width: wp('42%'),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F8F8F8',
      marginHorizontal: 5
   },
   card2Label: {
      fontFamily: 'Poppins-Medium',
      fontSize: 13,
      color: '#0A3977'
   },
   label3Style: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: '#414141'
   },
   ellipseImageContainer: {
      position: 'absolute',
      top: 0,
      left: 0
   },
   card4Container: {
      height: 125,
      borderWidth: 3,
      borderColor: '#C5DCFA',
      borderRadius: 10,
      marginVertical: 10
   },
   ellipse2ImageContainer: {
      position: 'absolute',
      top: 50,
      right: 0
   },
   card4Label: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      color: '#0A3977'
   },
   arrowButton: {
      justifyContent: 'flex-end',
      // borderWidth: 1,
      alignSelf: 'flex-end'
   },
   healthTipsCards: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
      elevation: 5,
      height: 150
   },
   view: {
      justifyContent: 'space-evenly',
      alignItems: 'center',
      // elevation: 1,
      width: wp('40%'),
      height: 180,
      borderRadius: 8,
      paddingHorizontal: 10,
      margin: 10,
      borderWidth: 1,
      borderColor: '#C4C4C4'
   },
   healthTipsStyle: {
      fontFamily: 'Poppins-Regular',
      fontSize: 10,
      textAlign: 'left',
      // borderWidth: 1,
      // width: '100%'
   },
   healthTipsStyle2: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      textAlign: 'left',
      // borderWidth: 1,
      // width: '100%'
   },
   tutorialView: {
      // flex: 1,
      height: hp('100'),
      justifyContent: 'center',
      alignItems: 'center',
      // borderWidth: 1
   },
   arrow1IconContainer: {
      // borderWidth: 1,
      top: hp('-3'),
      left: hp('5'),
      width: wp('70'),
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      position: 'absolute',
      elevation: 10
   },
   tutorialLabels: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      // color: 'white'
   },
   burgerMenu: {
      height: 280,
      width: 135,
      // margin: 10
      // borderWidth: 1
   },
   okayButtonConatiner: {
      // borderWidth: 1,
      width: wp('50'),
      height: hp('10'),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#777777',
      borderRadius: 20,
      elevation: 10
   },
   arrow2IconContainer: {
      position: 'absolute',
      bottom: hp('13'),
      left: hp('-1'),
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 10
   },
   homeScreenMenu: {
      width: 149,
      height: 431
   },
   shadow: {
      shadowColor: 'black',
      shadowOpacity: 0.5,
      shadowRadius: 5,
      // // iOS
      // shadowOffset: {
      //     width: 0,            // These can't both be 0
      //     height: 1,           // i.e. the shadow has to be offset in some way
      // },
      // Android
      shadowOffset: {
         width: 0,            // Same rules apply from above
         height: 1,           // Can't both be 0
      },
   }
});