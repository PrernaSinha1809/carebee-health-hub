import axios from 'axios';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5'
// import axios from 'axios';

class Profile extends Component {
   constructor(props) {
      super(props);
      this.state = {
         Name: '',
         Email: '',
         resourcePath: {},
      }
   }

   selectFile = () => {
      var options = {
         title: 'Select Image',
         customButtons: [
            {
               name: 'customOptionKey',
               title: 'Choose file from Custom Option'
            },
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
         },
      };

      ImagePicker.showImagePicker(options, res => {
         console.log('Response = ', res.path);

         if (res.didCancel) {
            console.log('User cancelled image picker');
         } else if (res.error) {
            console.log('ImagePicker Error: ', res.error);
         } else if (res.customButton) {
            console.log('User tapped custom button: ', res.customButton);
            alert(res.customButton);
         } else {
            let source = res;
            this.setState({
               resourcePath: source,
            });
         }
      });
   };

   componentDidMount = () => {
      // axios({
      //    method: 'GET',
      //    url: 'https://fcaaa1de09d1.ngrok.io/api/Carebee/603355ee51f4953174ed5782'
      // }).then((response) => response)
      //    .then((responseJson) => {
      //       console.log("responseJson.data", responseJson.data);
      //       this.setState({ ...responseJson.data })
      //    })
      //    .catch((error) => {
      //       console.error(error);
         // });
// -------------------------------------------Ignore------------------------------------
      // console.log("this.state", this.state)
      // fetch('https://c7cc7a9597d7.ngrok.io/api/Carebee/603355ee51f4953174ed5782', {
      //    method: 'GET'
      // })
      //    .then((response) => response.json())
      //    .then((responseJson) => {
      //       console.log(responseJson);
      //       this.setState({ ...responseJson })
      //    })
      //    .catch((error) => {
      //       console.error(error);
      //    });
   }
   render() {
      return (
         <View style={styles.container}>
            {/* -------------------------------Profile Header---------------------------------- */}
            <View style={{ position: 'absolute', left: 0, top: 0, borderWidth: 1, height: 50, width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>
               <View style={{ width: '30%', justifyContent: 'center', alignItems: 'flex-start' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Homescreen')}>
                     <Icon name="arrow-left" size={20} color='black' />
                  </TouchableOpacity>
               </View>
               <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Profile</Text>
               </View>
               <View style={{ width: '30%', justifyContent: 'center', alignItems: 'flex-end' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('EditProfile')}>
                     <Icon name="user-edit" size={20} color='#0c6873' />
                  </TouchableOpacity>
               </View>
            </View>
            {/* -------------------------------End of Profile Header---------------------------------- */}
            {(this.state.resourcePath.data == null) ? (
               <View style={{ borderWidth: 1, borderRadius: 100, height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity onPress={this.selectFile}>
                     <Image style={{ height: 100, width: 100 }} source={require('../assets/profile/person.png')} />
                  </TouchableOpacity>
               </View>
            ) :
               (<View style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 1 }}>
                  <TouchableOpacity onPress={this.selectFile}>
                     <Image
                        source={{
                           uri: 'data:image/jpeg;base64,' + this.state.resourcePath.data,
                        }}
                        style={{ width: 100, height: 100, borderRadius: 100 }}
                     />
                  </TouchableOpacity>
               </View>)}
            {/* <TouchableOpacity onPress={this.selectFile} style={styles.button}  >
               <Text style={styles.buttonText}>Select File</Text>
            </TouchableOpacity> */}
            <View style={{ flexDirection: 'row', height: 40, width: 200, justifyContent: 'center' }}>
               {/* <Text style={{ fontSize: 20 }}>Name:</Text> */}
               <Text style={{ fontSize: 20, textAlign: 'right' }}>{this.state.Name}</Text>
            </View>
            <View style={{ paddingHorizontal: 10 }}>
               <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Personal Info</Text>
                  <View
                     style={{
                        width: '60%',
                        height: 15,
                        justifyContent: 'center',
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                     }}
                  />
               </View>
               <View style={{ flexDirection: 'row', height: 40, width: '90%' }}>
                  <Text style={{ fontSize: 20 }}>Email: </Text>
                  <Text style={{ fontSize: 20, textAlign: 'right', width: '90%' }}>{this.state.Email}</Text>
               </View>
               <View style={{ flexDirection: 'row', height: 40, width: '90%' }}>
                  <Text style={{ fontSize: 20 }}>Phone Number: </Text>
                  <Text style={{ fontSize: 20, textAlign: 'right', width: '62%' }}>{this.state.PhoneNumber}</Text>
               </View>
               <View style={{ flexDirection: 'row', height: 40, width: '90%' }}>
                  <Text style={{ fontSize: 20 }}>Date Of Birth: </Text>
                  <Text style={{ fontSize: 20, textAlign: 'right', width: '68%' }}>{this.state.DateOfBirth}</Text>
               </View>
               <View style={{ flexDirection: 'row', height: 40, width: '90%' }}>
                  <Text style={{ fontSize: 20 }}>Weight: </Text>
                  <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Weight}</Text>
               </View>
               <View style={{ flexDirection: 'row', height: 40, width: '90%' }}>
                  <Text style={{ fontSize: 20 }}>Height: </Text>
                  <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Height}"</Text>
               </View>
               <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Additional Info</Text>
                  <View
                     style={{
                        width: '60%',
                        height: 15,
                        justifyContent: 'center',
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                     }}
                  />
               </View>
            </View>
         </View>
      );
   }
}

export default Profile

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
   },
});