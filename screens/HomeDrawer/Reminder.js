import React, { Component } from 'react';
import { View, Text, Platform, StyleSheet, TouchableOpacity, Animated, ScrollView, Switch } from 'react-native';
// import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Entypo';
import DateTimePickerModal from "react-native-modal-datetime-picker";

export default class Reminder extends Component {
  constructor() {
    super();

    this.state = {
      valueArray: [],
      disabled: false,
      toggle: false,
      isDatePickerVisible: false,
      pickedTime: '00:00:00',
      heightOfTimer: 80
    }

    this.index = 0;

    this.animatedValue = new Animated.Value(0);
  }

  showDatePicker = () => {
    this.setState({ isDatePickerVisible: true })
  }

  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false })
  }

  increaseHeight = () => {
    if (this.state.heightOfTimer == 80) {
      this.setState({ heightOfTimer: 150 })
    } else {
      this.setState({ heightOfTimer: 80 })
    }

  }

  handleConfirm = (date) => {
    var pickedTimeString = JSON.stringify(date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds())
    var stripped = pickedTimeString.replace(/\,(?!\s*?[\{\[\"\'\w])/g, '');
    var pickedTime = JSON.parse(stripped)
    this.setState({ pickedTime: pickedTime, toggle: true })
    // this.setState({})
    this.hideDatePicker();
  }

  handleSwitch = () => {
    // var switchState = this.state.toggle
    this.setState({ toggle: !this.state.toggle })
  }

  addMore = () => {
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index }

    this.setState({ disabled: true, valueArray: [...this.state.valueArray, newlyAddedValue] }, () => {
      Animated.timing(
        this.animatedValue,
        {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }
      ).start(() => {
        this.index = this.index + 1;
        this.setState({ disabled: false });
      });
    });
  }

  removeMore = (key) => {
    let valueArray = this.state.valueArray;
    // console.log(valueArray)
    valueArray.pop()
    // console.log(valueArray)
    // console.log(valueArray.splice(valueArray.indexOf(3)))
    // valueArray.pop()
    this.setState({ valueArray, pickedTime: '00 : 00 : 00', toggle: false })
  }

  render() {
    const animationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });

    let newArray = this.state.valueArray.map((item, key) => {
      // console.log(key)
      if ((key) == this.index) {
        return (
          <Animated.View key={key} style={[styles.viewHolder, { opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
            <Text style={styles.text}>{this.state.pickedTime}</Text>
          </Animated.View>
        );
      }
      else {
        return (

          <TouchableOpacity onPress={this.showDatePicker} key={key} style={[styles.viewHolder, { height: this.state.heightOfTimer }]}>
            <View style={styles.viewHolder1}>
              <View style={{justifyContent: 'center'}}>
              <DateTimePickerModal
                isVisible={this.state.isDatePickerVisible}
                mode="time"
                onConfirm={this.handleConfirm}
                onCancel={this.hideDatePicker}
                // display="inline"
              />
              <Text style={styles.reminderStyle}>{this.state.pickedTime}</Text>
              </View>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Switch
                  trackColor={{ false: 'gray', true: '#2ab930' }}
                  thumbColor="white"
                  ios_backgroundColor="gray"
                  onValueChange={(value) => this.setState({ toggle: value })}
                  value={this.state.toggle}
                />
                <TouchableOpacity onPress={this.increaseHeight}>
                  <Icon name="caret-down-outline" size={30} color='grey' />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
    });

    return (
      <View style={styles.container}>
        <Text style={styles.titleStyle}>Reminders</Text>
        <ScrollView>
          <View style={{ flex: 1, padding: 4 }}>
            {
              newArray
            }
          </View>
        </ScrollView>

        <TouchableOpacity activeOpacity={0.8} style={styles.btn} disabled={this.state.disabled} onPress={this.addMore}>
          <Icon name="add" size={30} color='white' />

        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.8} style={styles.btn1} disabled={this.state.disabled} onPress={this.removeMore}>
          <Icon1 name="minus" size={30} color='white' />

        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create(
  {
    container:
    {
      flex: 1,
      backgroundColor: '#eee',
      justifyContent: 'center',
      paddingTop: (Platform.OS == 'ios') ? 20 : 0
    },

    viewHolder:
    {
      // flexDirection: 'row',
      // height: this.state.heightOfTimer,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 10,
      margin: 4
    },
    viewHolder1: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      // borderWidth: 1,
      width: '80%'
    },
    text:
    {
      color: 'black',
      fontSize: 25,
      fontFamily: 'Poppins-Regular',
      // fontSize: 15,
      color: '#2ab930'
    },

    btn1:
    {
      position: 'absolute',
      left: 25,
      bottom: 25,
      borderRadius: 30,
      width: 60,
      height: 60,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.7)',
      padding: 15,
    },

    btn:
    {
      position: 'absolute',
      right: 25,
      bottom: 25,
      borderRadius: 30,
      width: 60,
      height: 60,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.7)',
      padding: 15,
    },
    btnImage:
    {
      resizeMode: 'contain',
      width: '100%',
      tintColor: 'white'
    },
    reminderStyle: {
      fontFamily: 'Poppins-Regular',
      fontSize: 20,
      color: '#2ab930'
    },
    titleStyle: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      textAlign: 'center',
      marginVertical: 10
    }
  });