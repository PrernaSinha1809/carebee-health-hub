import React from 'react';
import { Button, View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";

export default class MyPrescription extends React.Component {
   constructor(props){
      super(props);
      this.state={
         isDatePickerVisible: false,
         pickedTime: ''
      }
   }
   showDatePicker = () => {
      this.setState({isDatePickerVisible: true})
   }
   hideDatePicker = () => {
      this.setState({isDatePickerVisible: false})
   }

   handleConfirm = (date) => {
      var pickedTimeString = JSON.stringify(date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds())
      this.setState({pickedTime: pickedTimeString})
      this.hideDatePicker();
   }
   render() {
      return (
         <View style = {styles.container}>
            <View style={styles.blueViewHolder}>

            </View>
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
      // flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
   },
   blueViewHolder: {
      // flex: 1,
      height: '50%',
      borderWidth: 1,
      width: '100%',
      borderBottomEndRadius: 40,
      borderBottomLeftRadius: 40
   }
});