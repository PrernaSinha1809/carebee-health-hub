import { View } from 'native-base'
import React, { Component } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import { TouchableHighlight } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons';
import Blood_PressureIcon from '../assets/fonts/blood_pressure.svg'
import Blood_SugarIcon from '../assets/fonts/blood_sugar.svg'
import TemperatureIcon from '../assets/fonts/temperature.svg'
import Body_weightIcon from '../assets/fonts/body_weight.svg'
import Blood_OxygenIcon from '../assets/fonts/spo2.svg'
import Heart_RateIcon from '../assets/fonts/heart_rate.svg'

class Vitals extends Component {
    render() {
        return (
            <View style={styles.container} >
                <View style={{paddingVertical: 20}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: '70%', paddingLeft: 15, paddingTop: 15 }}>
                            <View><Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20, color: '#818181' }}>Add your</Text></View>
                            <View><Text style={{ fontFamily: 'Poppins-Regular', fontSize: 46, color: '#414141' }}>Vitals</Text></View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181' }}>Keep tracking your progress by adding and updating your progress daily.</Text></View>
                        </View>
                        <View style={{ width: '30%' }}>
                            <View>
                                <TouchableHighlight style={{ alignSelf: 'center', borderRadius: 20, backgroundColor: '#e0e0e0' }}>
                                    <Icon
                                        name="ios-add"
                                        color="#414141"
                                        size={100}
                                    />
                                </TouchableHighlight>
                            </View>
                            <View style={{ padding: 4 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Add New</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', paddingVertical: 30 }}>
                        <View style={{ backgroundColor: '#c7cdf1', borderRadius: 20, width: 100, height: 100 }}>
                            <View>
                                <Blood_PressureIcon style={{ alignSelf: 'center', margin: 20 }} />
                            </View>
                            <View style={{ marginTop: 3 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Blood Pressure</Text>
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#fac1d6', borderRadius: 20, width: 100, height: 100 }}>
                            <View>
                                <Blood_SugarIcon style={{ alignSelf: 'center', margin: 20 }} />
                            </View>
                            <View style={{ marginTop: 12 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Blood Sugar</Text>
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#c5dcfa', borderRadius: 20, width: 100, height: 100 }}>
                            <View>
                                <TemperatureIcon style={{ alignSelf: 'center', margin: 20 }} />
                            </View>
                            <View style={{ marginTop: 17 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Temperature</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <View style={{ width: 100, height: 100, borderRadius: 20, backgroundColor: '#c5f2c7' }}>
                            <View>
                                <Body_weightIcon style={{ alignSelf: 'center', margin: 25 }} />
                            </View>
                            <View style={{ marginTop: 17 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Body Weight</Text>
                            </View>
                        </View>
                        <View style={{ width: 100, height: 100, borderRadius: 20, backgroundColor: '#c7cdf1' }}>
                            <View>
                                <Blood_OxygenIcon style={{ alignSelf: 'center', margin: 30 }} />
                            </View>
                            <View style={{ marginTop: 17 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Blood Oxygen</Text>
                            </View>
                        </View>
                        <View style={{ width: 100, height: 100, borderRadius: 20, backgroundColor: '#fcd0cf' }}>
                            <View>
                                <Heart_RateIcon style={{ alignSelf: 'center', margin: 25 }} />
                            </View>
                            <View style={{ marginTop: 17 }}>
                                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#818181', textAlign: 'center' }}>Heart Rate</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default Vitals

const styles = StyleSheet.create({
    container: {
        //     flex: 1,
        //     backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});