import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    TextInput,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign'

import RNSpeedometer from 'react-native-speedometer'

export default class MentalHCalculator extends Component {
    state = {
        values: 0,
    };

    onChange = (values) => this.setState({ values: parseInt(values) });

    render() {
        // const { postId, otherParam } = this.props.route.params;
        const { value } = this.props.route.params;
        // console.log(value)
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.screenLabelContainer}>
                    <View style={styles.arrowContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Homescreen')}>
                            <Icon name="arrowleft" size={25} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text style={styles.screenLabelStyle}>Mental health meter</Text>
                    </View>
                </View>
                {/* <View>
                    <TextInput placeholder="Speedometer Value" style={styles.textInput} value={value} />
                </View> */}
                <View>
                    <RNSpeedometer value={parseInt(value)} size={300} />
                </View>
                <View style={styles.messageContainer}>
                    <Text style={styles.messageStyle}>
                        In case, you want to book an appointment with our mental health professional. Kindly reach out to us. <Text style={{ color: '#1672EC' }}>Feel free to click on the message button below to get professional’s advice.</Text>
                    </Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.messageButtonContainer}>
                        <Text style={styles.messageButtonStyle}>Message Us</Text>
                    </TouchableOpacity>
                </View>
                {/* <TouchableOpacity onPress={abc}>
                    <Text>
                        Show
                    </Text>
                </TouchableOpacity> */}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        borderBottomWidth: 0.3,
        borderBottomColor: 'black',
        height: heightPercentageToDP('8%'),
        fontSize: 25,
        // marginVertical: 50,
        // marginHorizontal: 20,
        color: 'black',
    },
    screenLabelContainer: {
        position: 'absolute',
        top: 0,
        width: widthPercentageToDP('100%'),
        justifyContent: 'center',
        alignItems: 'center',
        height: heightPercentageToDP('10%'),
        // borderWidth: 1,
        flexDirection: 'row'
    },
    screenLabelStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        color: '#414141'
    },
    arrowContainer: {
        position: 'absolute',
        left: 10
    },
    messageContainer: {
        // borderWidth: 1,
        width: widthPercentageToDP('85%'),
        justifyContent: 'center',
        alignItems: 'center',
        height: heightPercentageToDP('15%'),
        marginTop: heightPercentageToDP('20%')
    },
    messageStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 12,
        textAlign: 'center'
    },
    messageButtonContainer: {
        width: widthPercentageToDP('50%'),
        // borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: heightPercentageToDP('7%'),
        elevation: 5,
        backgroundColor: '#1672EC',
        borderRadius: 10,
        marginTop: heightPercentageToDP('2%')
    },
    messageButtonStyle: {
        fontFamily: 'Poppins-Medium',
        fontSize: 15,
        color: 'white'
    }
});