import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
// import { Constants } from 'expo';

// You can import from local files
import OtpInputs from './OtpInput';

// or any pure javascript modules available in npm
// import { Card } from 'react-native-paper';

export default class App extends React.Component {
  state={otp:''};
   getOtp(otp) {
        console.log(otp);
        this.setState({ otp });
  }
  render() {
    return (
      <View style={styles.container}>
      {/* <Text>OTP : {this.state.otp}</Text> */}
        <OtpInputs getOtp={(otp) => this.getOtp(otp)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    // paddingTop: Constants.statusBarHeight,
    // backgroundColor: '#ecf0f1',
    // padding: 8,
    // borderWidth: 2,
    height: 100,
    width: 200
  }
});
