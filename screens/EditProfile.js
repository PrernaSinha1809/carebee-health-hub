import axios from 'axios';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, SafeAreaView, StatusBar, ScrollView, Button } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5'

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Name: '',
            Email: '',
            resourcePath: {},
        }
    }

    selectFile = () => {
        var options = {
            title: 'Select Image',
            customButtons: [
                {
                    name: 'customOptionKey',
                    title: 'Choose file from Custom Option'
                },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, res => {
            console.log('Response = ', res.path);

            if (res.didCancel) {
                console.log('User cancelled image picker');
            } else if (res.error) {
                console.log('ImagePicker Error: ', res.error);
            } else if (res.customButton) {
                console.log('User tapped custom button: ', res.customButton);
                alert(res.customButton);
            } else {
                let source = res;
                this.setState({
                    resourcePath: source,
                });
            }
        });
    };

    handleUpdate = () => {
        // console.log('-----------------',this.state)
        // axios({
        //     method: 'PUT',
        //     url: 'https://fcaaa1de09d1.ngrok.io/api/Carebee/update/603355ee51f4953174ed5782',
        //     data: this.state
        // }).then(res => console.log(res.data))
        // .catch(err => console.log(err));
        // this.props.navigation.navigate('Profile', {
        //     onGoBack: () => this.refresh(),
        //   })
    }

    componentDidMount = () => {
        // axios({
        //     method: 'GET',
        //     url: 'https://fcaaa1de09d1.ngrok.io/api/Carebee/603355ee51f4953174ed5782'
        // }).then((response) => response)
        //     .then((responseJson) => {
        //         console.log(responseJson.data);
        //         this.setState({ ...responseJson.data })
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });
    }
    render() {
        return (
            <View>
                {/* -------------------Top Bar------------------------------------------ */}
                <View style={{ height: 50, width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                            <Icon name="arrow-left" size={20} color='black' />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Edit Profile</Text>
                    </View>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'flex-end' }}>
                        {/* <Icon name="user-edit" size={20} color='#0c6873' /> */}
                    </View>
                </View>
                {/* --------------------------Scrollable Details------------------------------ */}
                <ScrollView >
                    <View style={{ marginVertical: 30 }}>
                        {/* --------------------Profile Photo Start----------------------------- */}
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginVertical: 20 }}>
                            {(this.state.resourcePath.data == null) ? (
                                <View style={{ borderWidth: 1, borderRadius: 100, height: 100, width: 100, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity onPress={this.selectFile}>
                                        <Image style={{ height: 100, width: 100 }} source={require('../assets/profile/person.png')} />
                                    </TouchableOpacity>
                                </View>
                            ) :
                                (<View style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 1 }}>
                                    <TouchableOpacity onPress={this.selectFile}>
                                        <Image
                                            source={{
                                                uri: 'data:image/jpeg;base64,' + this.state.resourcePath.data,
                                            }}
                                            style={{ width: 100, height: 100, borderRadius: 100 }}
                                        />
                                    </TouchableOpacity>
                                </View>)}
                            {/* --------------------Profile Photo End----------------------------- */}
                        </View>
                        <View style={{ height: 40, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Text style={{ fontSize: 20 }}>Name:</Text> */}
                            <Text style={{ fontSize: 20 }}>{this.state.Name}</Text>
                        </View>
                        {/* ----------------------------Personal Details Form Start------------------------------ */}
                        <View style={{ width: '100%', paddingHorizontal: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Personal Info</Text>
                                <View
                                    style={{
                                        width: '60%',
                                        height: 15,
                                        justifyContent: 'center',
                                        borderBottomColor: 'black',
                                        borderBottomWidth: 1,
                                    }}
                                />
                            </View>
                            <View style={{ paddingHorizontal: 10 }}>
                                <View style={{ width: '90%', marginVertical: 5 }}>
                                    <Text style={styles.formLabel}>Email: </Text>
                                    {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '90%' }}>{this.state.Email}</Text> */}
                                    <View style={{ width: 380, borderBottomWidth: 1 }}>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            onChangeText={text => this.setState({Email: text})}
                                            value={this.state.Email}
                                        />
                                    </View>
                                </View>
                                <View style={{ width: '90%' }}>
                                    <Text style={styles.formLabel}>Phone Number: </Text>
                                    {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '62%' }}>{this.state.PhoneNumber}</Text> */}
                                    <View style={{ width: 380, borderBottomWidth: 1 }}>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            onChangeText={text => this.setState({PhoneNumber: text})}
                                            value={this.state.PhoneNumber}
                                        />
                                    </View>
                                </View>
                                <View style={{ width: '90%' }}>
                                    <Text style={styles.formLabel}>Date Of Birth: </Text>
                                    {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '68%' }}>{this.state.DateOfBirth}</Text> */}
                                    <View style={{ width: 380, borderBottomWidth: 1 }}>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            onChangeText={text => this.setState({DateOfBirth: text})}
                                            value={this.state.DateOfBirth}
                                        />
                                    </View>
                                </View>
                                <View style={{ width: '90%' }}>
                                    <Text style={styles.formLabel}>Weight: </Text>
                                    {/* <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Weight}</Text> */}
                                    <View style={{ width: 380, borderBottomWidth: 1 }}>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            onChangeText={text => this.setState({Weight: text})}
                                            value={this.state.Weight}
                                        />
                                    </View>
                                </View>
                                <View style={{ width: '90%' }}>
                                    <Text style={styles.formLabel}>Height: </Text>
                                    {/* <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Height}"</Text> */}
                                    <View style={{ width: 380, borderBottomWidth: 1 }}>
                                        <TextInput
                                            style={styles.textInputStyle}
                                            onChangeText={text => this.setState({Height: text})}
                                            value={this.state.Height}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/* ----------------------------Personal Details Form End------------------------------ */}
                        {/* --------------------------------------Additional Details Form Start-------------------------------- */}
                        <View style={{ padding: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 20 }}>Additional Info</Text>
                                <View
                                    style={{
                                        width: '60%',
                                        height: 15,
                                        justifyContent: 'center',
                                        borderBottomColor: 'black',
                                        borderBottomWidth: 1,
                                    }}
                                />
                            </View>
                            <View style={{ width: '90%', marginVertical: 5 }}>
                                <Text style={styles.formLabel}>Email: </Text>
                                {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '90%' }}>{this.state.Email}</Text> */}
                                <View style={{ width: 380, borderBottomWidth: 1 }}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        onChangeText={text => this.setState({Email: text})}
                                        value={this.state.Email}
                                    />
                                </View>
                            </View>
                            <View style={{ width: '90%' }}>
                                <Text style={styles.formLabel}>Phone Number: </Text>
                                {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '62%' }}>{this.state.PhoneNumber}</Text> */}
                                <View style={{ width: 380, borderBottomWidth: 1 }}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        onChangeText={text => this.setState({PhoneNumber: text})}
                                        value={this.state.PhoneNumber}
                                    />
                                </View>
                            </View>
                            <View style={{ width: '90%' }}>
                                <Text style={styles.formLabel}>Date Of Birth: </Text>
                                {/* <Text style={{ fontSize: 20, textAlign: 'right', width: '68%' }}>{this.state.DateOfBirth}</Text> */}
                                <View style={{ width: 380, borderBottomWidth: 1 }}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        // onChangeText={text => onChangeText(text)}
                                        onChangeText={text => this.setState({DateOfBirth: text})}
                                        value={this.state.DateOfBirth}
                                    />
                                </View>
                            </View>
                            <View style={{ width: '90%' }}>
                                <Text style={styles.formLabel}>Weight: </Text>
                                {/* <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Weight}</Text> */}
                                <View style={{ width: 380, borderBottomWidth: 1 }}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        // onChangeText={text => onChangeText(text)}
                                        onChangeText={text => this.setState({Weight: text})}
                                        value={this.state.Weight}
                                    />
                                </View>
                            </View>
                            <View style={{ width: '90%' }}>
                                <Text style={styles.formLabel}>Height: </Text>
                                {/* <Text style={{ fontSize: 20, textAlign: 'right', width: "85%" }}>{this.state.Height}"</Text> */}
                                <View style={{ width: 380, borderBottomWidth: 1 }}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        // onChangeText={text => onChangeText(text)}
                                        onChangeText={text => this.setState({Height: text})}
                                        value={this.state.Height}
                                    />
                                </View>
                            </View>
                            <View style={{ marginBottom: 50, alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity onPress={this.handleUpdate} style={{ height: 50, width: '30%', backgroundColor: '#68bdc7', borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: '#009e8e', fontFamily: 'Poppins-Medium', fontSize: 15, textAlign: 'center' }}>Save</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* --------------------------------------Additional Details Form End-------------------------------- */}

                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default EditProfile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formLabel: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium',
        color: '#a1a1a1'
    },
    textInputStyle: {
        width: '100%',
        borderColor: 'gray',
        textAlign: 'auto',
        fontSize: 16,
        fontFamily: 'Poppins-Medium',
        color: '#414141'
    }

});